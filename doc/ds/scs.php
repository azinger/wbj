<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- InstanceBegin template="/Templates/tpex_main_php.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Selected Cities - NOAA's National
Weather Service</title>
<meta name="DC.title" content="Selected Cities - NOAA's National Weather Service" />
<meta name="DC.description" content="Selected Cities" />
<meta name="DC.subject" content="Selected Cities" />
<meta name="DC.date.reviewed" scheme="ISO8601" content="2008-12-03" />
<meta name="DC.date.created" scheme="ISO8601" content="2008-12-03" />
<!-- InstanceEndEditable -->
<meta name="DC.format" content="text/html; charset=iso-8859-1" />
<meta name="DC.language" scheme="DCTERMS.RFC1766" content="EN-US" />
<meta name="DC.Distribution" content="Global" />
<meta name="DC.robot" content="all" />
<meta name="DC.creator" content="NOAA's National Weather Service" />
<meta name="DC.contributor" content="NWS Internet Services Team" />
<link rel="schema.DC" href="http://purl.org/dc/elements/1.1/" />
<link rel="schema.DCTERMS" href="http://purl.org/dc/terms/" />
<link rel="DC.rights" href="http://www.weather.gov/disclaimer.php" />
<link rel="stylesheet" type="text/css" href="/main.css" />
<link href="/favicon.ico" rel="shortcut icon" />
<script type="text/javascript" language="JavaScript" src="/master.js"></script>
</head>
<body onload="init()" background="/images/background1.gif">
<!-- Start banner -->
<!-- start banner inc -->
<table cellspacing="0" cellpadding="0" border="0" width="100%" background="/images/topbanner.jpg">
  <tr>
    <td align="right" height="19"><a href="#contents"><img src="/images/skipgraphic.gif" alt="Skip Navigation Links" width="1" height="1" border="0" /></a> <a href="/"><span class="nwslink">weather.gov</span></a>&nbsp;</td>
  </tr>
</table>
<table cellspacing="0" cellpadding="0" border="0" width="100%">
  <tr>
    <td width="85" rowspan="2"><a href="http://www.noaa.gov"><img src="/images/noaaleft.jpg" alt="NOAA logo-Select to go to the NOAA homepage" width="85" height="78" border="0" /></a></td>
    <td width="500" height="20" align="center" nowrap="nowrap" background="/officenames/blank_title.jpg"><div class="source">National Oceanic and Atmospheric Administration's&nbsp;&nbsp;&nbsp;</div></td>
    <td rowspan="2" background="/images/wfo_bkgrnd.jpg">&nbsp;</td>
    <td width="85" rowspan="2" align="right"><a href="/"><img src="/images/nwsright.jpg" alt="Select to go to the NWS homepage" width="85" height="78" border="0" /></a></td>
  </tr>
  <tr>
    <td width="500" height="58" align="center" nowrap="nowrap" background="/officenames/blank_name.jpg"><div class="location">National Weather Service&nbsp;&nbsp;&nbsp;</div></td>
  </tr>
</table>
<table cellspacing="0" cellpadding="0" border="0" background="/images/navbkgrnd.gif" width="100%">
  <tr>
    <td align="left" valign="top" width="94"><img src="/images/navbarleft.jpg" alt="" width="94" height="23" border="0" /></td>
    <td class="nav" width="15%" align="center" id="menuitem" nowrap="nowrap"><a href="/sitemap.php">Site Map</a></td>
    <td class="nav" width="15%" align="right" id="menuitem"><a href="/pa/">News</a></td>
    <td class="nav" width="20%" align="right" id="menuitem"><a href="/organization.php">Organization</a> </td>
    <td width="20%">&nbsp;</td>
    <td align="left" class="searchinput" width="20%" nowrap="nowrap">    
    <!-- Begin search code -->
      <form method="get" action="http://search.usa.gov/search" style="margin-bottom:0; margin-top:0;">
        <input type="hidden" name="v:project" value="firstgov" />
        <label for="query" class="yellow">Search&nbsp;&nbsp;</label>
        <input type="text" name="query" id="query" size="10"/>
        <input type="radio" name="affiliate" checked="checked" value="nws.noaa.gov" id="nws" />
        <label for="nws" class="yellow">NWS</label>
        <input type="radio" name="affiliate" value="noaa.gov" id="noaa" />
        <label for="noaa" class="yellow">All NOAA</label>
        <input type="submit" value="Go" />
      </form>
      <!-- End search code -->
    </td>
    <td width="10%">&nbsp;</td>
    <td align="right" valign="bottom" width="24"><img src="/images/navbarendcap.jpg" alt="" width="24" height="23" border="0" /></td>
  </tr>
</table>
<!-- end banner inc --><!-- end banner -->
<table width="700" border="0" cellspacing="0">
  <tr>
    <td width="119" valign="top"><!-- start leftmenu -->
      <!-- ========== start leftmenu inc ========== -->

<table width="119" border="0" cellpadding="2" cellspacing="0" bgcolor="#0A2390">
  <!-- ********** local forecast search ********** -->
  <tr>
    <td class="yellow" align="center">Local forecast by<br />
      &quot;City, St&quot;</td>
  </tr>
  <tr align="center" valign="top">
    <td align="left" valign="bottom" nowrap="nowrap" class="searchinput"><form method="post" action="http://www.crh.noaa.gov/zipcity.php" style="margin-bottom:0; margin-top:0; ">
        <div id="Layer2" style="position:absolute; width:200px;  height:115px; z-index:2; visibility: hidden">Search
          by city. Press enter or select the go button to submit request</div>
        <input type="text" id="zipcity" name="inputstring" size="9" value="City, St" />
        <input type="submit" name="Go2" value="Go" />
      </form></td>
  </tr>
  <!-- ********** end local forecast search ********** -->
  <tr>
    <td class="white" id="menuitem">&#160;<a href="/xml/index.php#xml_changes"><img src="/images/xml.gif" alt="RSS Feeds" title="RSS Feeds" width="36" height="14" border="0" /> RSS Feeds</a></td>
  </tr>
  <tr>
    <td class="white" id="menuitem"><span class="yellow">TPEX Program</span><br />
      &#160;<a href="/xml/tpex/">TPEX Introduction</a><br />
      &#160;<a href="/xml/tpex/tpex_xml_definition.php">What is XML?</a><br />
      &#160;<a href="/xml/tpex/tpex_product_suite.php">Product Suite</a><br />
      &#160;<a href="/xml/tpex/tpex_xml_product_advantages.php">XML Products<br />
      &#160;Advantages</a><br />
      &#160;<a href="/xml/tpex/tpex_wmo_head_awips_id.php">WMO Headers<br />
      &#160;/AWIPS IDs</a><br />
      &#160;<a href="/xml/tpex/tpex_prod_dissm_meth.php">Product <br />
      &#160;Dissemination <br />
      &#160;Methods</a><br />
      &#160;<a href="/xml/tpex/faqs.php">FAQ's</a></td>
  </tr>
  <tr>
    <td class="white" id="menuitem"><span class="yellow">Operational XML Products</span><br />
      &#160;<a href="/view/xml_view2.php?tname=XF0&amp;prodver=1">Current FOX3</a><br />
      &#160;<a href="http://weather.noaa.gov/pub/SL.us008001/DF.xml/DC.textf/DS.xforx7/"><!--/sn.0000.txt -->Current FOX7</a><br />
      &#160;<a href="/view/xml_view2.php?tname=XOB&amp;prodver=1">Current ObX</a><br />
      &#160;<a href="/view/xml_view2.php?tname=XTE&amp;prodver=1">Current TEX</a></td>
  </tr>
  <tr>
    <td class="white" id="menuitem"><span class="yellow">Legacy Text Products</span><br />
      &#160;<a href="/xml/tpex/scs_tav_text_products.php">Operational SCS</a><br />
      &#160;&#160; - <a href="/data/WBN/SCS01">Part 1</a><br />
      &#160;&#160; - <a href="/data/WBN/SCS02">Part 2</a><br />
      &#160;&#160; - <a href="/data/WBN/SCS03">Part 3</a><br />
      &#160;&#160; - <a href="/data/WBN/SCS04">Part 4</a><br />
      &#160;&#160; - <a href="/xml/tpex/scs.php">Parts 1-4</a></td>
  </tr>
  <tr>
    <td class="white" id="menuitem"><a href="/xml/tpex/xslt.php"><span class="yellow">XSLT
      Stylesheets</span></a><br />
      &#160;<a href="/xml/tpex/samples/NaturalViewFoX.xsl">Natural View FOX<br />
      &#160;Stylesheet</a><br />
      &#160;<a href="/xml/tpex/samples/NaturalViewObX.xsl">Natural View ObX<br />
      &#160;Stylesheet</a><br />
      &#160;<a href="/xml/tpex/samples/NaturalViewTEX.xsl">Natural View TEX<br />
      &#160;Stylesheet</a></td>
  </tr>
  <tr>
    <td class="white" id="menuitem"><a href="/forecasts/xml/DWMLgen/schema/DWML.xsd"><span class="yellow">XML Schema</span></a></td>
  </tr>
  <tr>
    <td class="white" id="menuitem"><a href="/xml/"><span class="yellow">NDFD
      XML/SOAP Service</span></a></td>
  </tr>
  <tr>
    <td class="white" id="menuitem"><a href="/feedback.php"><span class="yellow">Contact
      Us</span></a><br />
      &#160;<a href="../../feedback.php">Comments</a><br />
    </td>
  </tr>
  <tr>
    <td class="white" id="menuitem">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" class="white" id="menuitem"><a href="http://www.usa.gov/"><img src="/images/usagov_logo_color_110w.gif" alt="USA.gov is the U.S. government's official web portal to all federal, state and local government web resources and services." title="USA.gov is the U.S. government's official web portal to all federal, state and local government web resources and services." width="110" height="30" border="0" /></a></td>
  </tr>
</table>
<!-- ========== end leftmenu inc ========== -->
      <!-- end leftmenu -->
    </td>
    <td width="525" valign="top"><!-- Start Content area... 525px wide -->
      <!-- InstanceBeginEditable name="Content" -->
      <table width="525" border="0" cellpadding="0" cellspacing="2">
        <tr>
          <td width="10" rowspan="2">&nbsp;</td>
          <td>&nbsp;&nbsp;<a href="/">Home</a> &gt; <a href="/xml/">XML</a> &gt; <a href="index.php">TPEX</a> &gt;  Selected
            Cities<br />
            <br />
          </td>
        </tr>
        <tr>
          <td align="center"><!--  *****************  START CONTENT SECTION  *************   -->
            <table width="400" border="0" align="center" cellpadding="10" cellspacing="2">
              <tr align="center">
                <td><hr width="90%" />
                  <span class="pagetitle">Selected Cities</span>
                  <hr width="90%" /></td>
              </tr>
              <tr>
                <td align="left">
<br/><pre>SELECTED CITIES WEATHER SUMMARY AND FORECASTS...PART 1 OF 4
NWS/NDFD TELECOMMUNICATION OPERATIONS CENTER SILVER SPRING MD
850 AM EDT SAT OCT 30 2010

TEMPERATURES INDICATE DAYTIME HIGH...NIGHTTIME LOW
B INDICATES TEMPERATURES BELOW ZERO
PRECIPITATION FOR 24 HOURS ENDING AT 8 AM EDT

                                FORECAST        FORECAST
                 FRI...OCT 29   SAT....OCT 30   SUN....OCT 31
CITY             HI/LO   PCPN   WEA     HI/LO   WEA     HI/LO

ABILENE TX       72  53         SUNNY   83/53   SUNNY   89/48
AKRON CANTON     46  33         PTCLDY  60/38   MOCLDY  50/31
ALBANY NY        51  32   .06   MOCLDY  54/38   PTCLDY  48/27
ALBUQUERQUE      69  46         PTCLDY  74/46   PTCLDY  67/41
ALLENTOWN        54  33   .01   PTCLDY  58/39   PTCLDY  55/29
AMARILLO         75  53         SUNNY   86/46   SUNNY   75/37
ANCHORAGE        38  31   .01   FLRRYS  33/27   MOCLDY  34/26
ASHEVILLE        61  31         SUNNY   65/34   SUNNY   69/39
ATLANTA          68  42         SUNNY   71/48   SUNNY   74/52
ATLANTIC CITY    59  39         PTCLDY  57/47   SUNNY   63/33
AUSTIN           73  32         SUNNY   79/45   SUNNY   86/59
BALTIMORE        57  35         PTCLDY  59/43   SUNNY   61/37
BATON ROUGE      74  39         SUNNY   75/49   SUNNY   81/61
BILLINGS         66  38         MOCLDY  57/38   MOCLDY  62/35
BIRMINGHAM       67  40         SUNNY   73/45   SUNNY   75/51
BISMARCK         49  33         MOCLDY  43/26   MOCLDY  46/32
BOISE            66  42         MOCLDY  59/45   SHWRS   57/43
BOSTON           58  40         MOCLDY  57/43   PTCLDY  56/34
BRIDGEPORT       56  42         MOCLDY  56/44   PTCLDY  56/30
BROWNSVILLE      80  53         SUNNY   83/62   PTCLDY  89/72
BUFFALO          49  40         WINDY   54/38   MOCLDY  44/31
BURLINGTON VT    46  34   .08   RAIN    46/42   MOCLDY  42/29
CARIBOU          52  32   .10   PTCLDY  39/21   CLOUDY  38/28
CASPER           66  33         PTCLDY  63/37   PTCLDY  56/35
CHARLESTON SC    72  44         SUNNY   73/53   SUNNY   80/54
CHARLESTON WV    55  30         SUNNY   67/40   PTCLDY  62/35
CHARLOTTE        66  41         SUNNY   67/41   SUNNY   73/45
CHATTANOOGA      65  37         SUNNY   70/41   SUNNY   71/46
CHEYENNE         70  42         PTCLDY  65/38   PTCLDY  57/38
CHICAGO          50  40         SUNNY   59/39   PTCLDY  51/39
CINCINNATI       53  29         SUNNY   64/39   PTCLDY  60/35
CLEVELAND        49  38         PTCLDY  62/40   MOCLDY  50/34
COLORADO SPGS    79  47         PTCLDY  70/36   PTCLDY  64/33
COLUMBIA SC      70  40         SUNNY   72/45   SUNNY   78/46
COLUMBUS GA      73  44         SUNNY   74/48   SUNNY   78/54
COLUMBUS OH      50  33         SUNNY   61/40   PTCLDY  53/33


$$<br/>SELECTED CITIES WEATHER SUMMARY AND FORECASTS...PART 2 OF 4
NWS/NDFD TELECOMMUNICATION OPERATIONS CENTER SILVER SPRING MD
850 AM EDT SAT OCT 30 2010

TEMPERATURES INDICATE DAYTIME HIGH...NIGHTTIME LOW
B INDICATES TEMPERATURES BELOW ZERO
PRECIPITATION FOR 24 HOURS ENDING AT 8 AM EDT

                                FORECAST        FORECAST
                 FRI...OCT 29   SAT....OCT 30   SUN....OCT 31
CITY             HI/LO   PCPN   WEA     HI/LO   WEA     HI/LO

CONCORD NH       55  27         MOCLDY  55/36   MOCLDY  47/27
CORPUS CHRISTI   75  42         SUNNY   80/59   PTCLDY  86/69
DALLAS FT WORTH  70  45         SUNNY   77/51   SUNNY   83/55
DAYTON           52  34         SUNNY   62/37   SUNNY   54/34
DAYTONA BEACH    81  67         PTCLDY  79/63   SUNNY   83/59
DENVER           79  44         MOCLDY  74/40   PTCLDY  65/39
DES MOINES       63  49         SUNNY   64/38   MOCLDY  52/34
DETROIT          51  39         WINDY   61/36   PTCLDY  52/34
DULUTH           37  33         MOCLDY  42/27   PTCLDY  41/30
EL PASO          80  52         SUNNY   84/52   SUNNY   81/50
ELKINS           44  24         SUNNY   61/33   MOCLDY  55/26
ERIE             47  40   .01   WINDY   59/42   MOCLDY  47/34
EUGENE           56  45         RAIN    56/46   SHWRS   58/45
EVANSVILLE       57  29         SUNNY   66/39   SUNNY   66/39
FAIRBANKS        26  23   .15   MOCLDY  25/01   MOCLDY  16/00
FARGO            44  33         MOCLDY  43/28   PTCLDY  45/33
FLAGSTAFF        66  30         WINDY   59/27   SUNNY   56/26
FLINT            50  40         PTCLDY  58/31   PTCLDY  47/30
FORT SMITH       67  34         SUNNY   73/41   SUNNY   76/47
FORT WAYNE       52  31         SUNNY   61/37   PTCLDY  53/35
FRESNO           78  55   .20   RAIN    61/48   SUNNY   70/50
GOODLAND         77  38         PTCLDY  74/38   PTCLDY  60/36
GRAND JUNCTION   60  36         MOCLDY  65/40   PTCLDY  59/36
GRAND RAPIDS     49  40         WINDY   57/33   MOCLDY  48/32
GREAT FALLS      60  31         PTCLDY  59/38   MOCLDY  56/38
GREEN BAY        46  40         MOCLDY  52/30   SUNNY   47/30
GREENSBORO       62  37         SUNNY   65/43   SUNNY   71/43
HARRISBURG       55  35         SUNNY   58/41   PTCLDY  56/35
HARTFORD SPGFLD  53  36         MOCLDY  58/38   PTCLDY  54/29
HELENA           59  28         MOCLDY  58/33   MOCLDY  54/33
HONOLULU         83  74         SUNNY   86/74   SUNNY   86/71
HOUSTON INTCNTL  77  45         SUNNY   80/53   SUNNY   84/67
HUNTSVILLE AL    63  34         SUNNY   71/42   SUNNY   73/46
INDIANAPOLIS     53  35         SUNNY   62/39   PTCLDY  58/35
JACKSON MS       67  36         SUNNY   75/44   SUNNY   77/55
JACKSONVILLE     78  50         PTCLDY  76/56   SUNNY   80/57
JUNEAU           46  38   .45   SHWRS   45/33   RAIN    41/34
KANSAS CITY      65  49         SUNNY   69/43   PTCLDY  60/41
KEY WEST         86  76   .01   PTCLDY  83/75   PTCLDY  84/75
KNOXVILLE        60  35         SUNNY   69/40   SUNNY   68/41
LAKE CHARLES     76  40         SUNNY   74/52   PTCLDY  80/64
LANSING          50  39         MOCLDY  57/33   PTCLDY  48/31
LAS VEGAS        75  56         WINDY   70/49   SUNNY   70/51
LEXINGTON        54  32         SUNNY   66/41   PTCLDY  61/42


$$<br/>SELECTED CITIES WEATHER SUMMARY AND FORECASTS...PART 3 OF 4
NWS/NDFD TELECOMMUNICATION OPERATIONS CENTER SILVER SPRING MD
850 AM EDT SAT OCT 30 2010

TEMPERATURES INDICATE DAYTIME HIGH...NIGHTTIME LOW
B INDICATES TEMPERATURES BELOW ZERO
PRECIPITATION FOR 24 HOURS ENDING AT 8 AM EDT

                                FORECAST        FORECAST
                 FRI...OCT 29   SAT....OCT 30   SUN....OCT 31
CITY             HI/LO   PCPN   WEA     HI/LO   WEA     HI/LO

LINCOLN          72  39         PTCLDY  67/35   MOCLDY  57/33
LITTLE ROCK      63  34         SUNNY   71/42   SUNNY   76/51
LOS ANGELES      79  58   .10   RAIN    69/59   SUNNY   71/59
LOUISVILLE       59  35         SUNNY   68/44   PTCLDY  64/44
LUBBOCK          74  46         SUNNY   89/47   WINDY   82/42
MACON            74  39         SUNNY   75/43   SUNNY   77/48
MADISON          50  43         PTCLDY  58/30   SUNNY   49/30
MEDFORD          61  49         MOCLDY  63/47   RAIN    55/44
MEMPHIS          63  38         SUNNY   71/48   SUNNY   75/57
MIAMI BEACH      90  75         PTCLDY  84/72   PTCLDY  85/72
MIDLAND ODESSA   76  48         SUNNY   86/47   SUNNY   87/44
MILWAUKEE        51  43         PTCLDY  58/36   SUNNY   48/38
MPLS ST PAUL     53  41         MOCLDY  50/30   PTCLDY  48/31
MISSOULA         46  36         MOCLDY  54/36   MOCLDY  45/34
MOBILE           73  44         SUNNY   75/54   SUNNY   80/60
MONTGOMERY       71  39         SUNNY   76/48   SUNNY   79/52
NASHVILLE        61  31         SUNNY   69/40   SUNNY   70/42
NEW ORLEANS      73  50         SUNNY   73/57   SUNNY   81/62
NEW YORK CITY    56  42         MOCLDY  59/44   PTCLDY  56/35
NEWARK           60  38         MOCLDY  59/44   PTCLDY  57/34
NORFOLK VA       61  49         SUNNY   62/49   SUNNY   70/47
NORTH PLATTE     74  33         PTCLDY  66/36   MOCLDY  56/35
OKLAHOMA CITY    69  46         SUNNY   75/45   SUNNY   75/45
OMAHA            70  41         PTCLDY  66/35   MOCLDY  54/34
ORLANDO          85  62         PTCLDY  80/61   SUNNY   86/60
PADUCAH          59  29         SUNNY   68/40   SUNNY   69/42
PENDLETON        57  34   .03   RAIN    54/42   MOCLDY  55/42
PEORIA           53  39         PTCLDY  63/39   PTCLDY  56/37
PHILADELPHIA     58  41         PTCLDY  59/47   SUNNY   59/37
PHOENIX          94  64         SUNNY   87/60   SUNNY   81/57
PITTSBURGH       46  33         SUNNY   58/40   MOCLDY  50/31
POCATELLO        61  27         MOCLDY  56/35   MOCLDY  51/32
PORTLAND ME      55  33   .01   MOCLDY  53/41   MOCLDY  53/29
PORTLAND OR      59  44         RAIN    55/48   SHWRS   57/50
PROVIDENCE       59  39         PTCLDY  57/45   PTCLDY  55/32
PUEBLO           83  49         PTCLDY  77/32   PTCLDY  67/28
RALEIGH DURHAM   62  36         SUNNY   66/44   SUNNY   73/44
RAPID CITY       72  36         PTCLDY  58/36   CLOUDY  54/35
RENO             64  43         MOCLDY  56/35   PTCLDY  60/37
RICHMOND         61  35         SUNNY   64/44   SUNNY   70/40
ROANOKE          58  35         SUNNY   65/46   SUNNY   69/43
ROCHESTER NY     47  36   .08   MOCLDY  55/39   MOCLDY  45/31
ROCKFORD         51  38         SUNNY   58/33   PTCLDY  50/34
SACRAMENTO       66  56   .01   MOCLDY  63/49   PTCLDY  67/48
ST LOUIS         60  38         SUNNY   71/44   SUNNY   62/44
ST PETERSBURG    86  70         SUNNY   82/68   SUNNY   83/69
ST THOMAS VI     84  77         PTCLDY  87/78   MOCLDY  87/78


$$<br/>SELECTED CITIES WEATHER SUMMARY AND FORECASTS...PART 4 OF 4
NWS/NDFD TELECOMMUNICATION OPERATIONS CENTER SILVER SPRING MD
850 AM EDT SAT OCT 30 2010

TEMPERATURES INDICATE DAYTIME HIGH...NIGHTTIME LOW
B INDICATES TEMPERATURES BELOW ZERO
PRECIPITATION FOR 24 HOURS ENDING AT 8 AM EDT

                                FORECAST        FORECAST
                 FRI...OCT 29   SAT....OCT 30   SUN....OCT 31
CITY             HI/LO   PCPN   WEA     HI/LO   WEA     HI/LO

SALEM OR         61  41         RAIN    56/46   SHWRS   58/48
SALT LAKE CITY   69  46         SHWRS   61/41   MOCLDY  55/36
SAN ANGELO       76  43         SUNNY   85/45   SUNNY   92/48
SAN ANTONIO      75  40         SUNNY   78/48   SUNNY   86/60
SAN DIEGO        78  64         MOCLDY  66/59   PTCLDY  69/58
SAN FRANCISCO    63  52   .09   MOCLDY  68/57   PTCLDY  69/58
SAN JOSE         67  53         MOCLDY  67/52   PTCLDY  70/54
SAN JUAN PR      86  76  1.82   PTCLDY  85/78   MOCLDY  84/78
SANTA FE         69  37         PTCLDY  73/38   PTCLDY  65/33
ST STE MARIE     47  39         RAIN    45/30   MOCLDY  41/29
SAVANNAH         77  44         SUNNY   75/50   SUNNY   80/51
SEATTLE          58  42         RAIN    55/49   MOCLDY  56/48
SHREVEPORT       69  35         SUNNY   74/49   PTCLDY  81/63
SIOUX CITY       71  43         SUNNY   60/34   MOCLDY  49/32
SIOUX FALLS      64  33         SUNNY   53/30   MOCLDY  48/32
SOUTH BEND       51  38         SUNNY   60/36   PTCLDY  52/35
SPOKANE          46  39   .23   MOCLDY  48/41   RAIN    50/38
SPRINGFIELD IL   56  43         SUNNY   64/40   PTCLDY  59/39
SPRINGFIELD MO   63  39         SUNNY   68/39   SUNNY   68/42
SYRACUSE         47  37   .13   MOCLDY  55/40   MOCLDY  43/31
TALLAHASSEE      77  45         SUNNY   78/46   SUNNY   80/49
TAMPA            87  64         SUNNY   83/64   SUNNY   83/65
TOLEDO           52  37         WINDY   62/36   PTCLDY  53/32
TOPEKA           68  50         SUNNY   70/41   PTCLDY  64/41
TUCSON           94  57         SUNNY   87/51   SUNNY   80/48
TULSA            68  48         SUNNY   75/46   SUNNY   75/45
TUPELO           64  33         SUNNY   71/41   SUNNY   75/47
WACO             72  37         SUNNY   79/48   SUNNY   85/55
WASHINGTON DC    58  40         SUNNY   60/48   SUNNY   61/39
W PALM BEACH     87  73   .39   PTCLDY  83/70   PTCLDY  84/71
WICHITA          70  45         SUNNY   72/44   SUNNY   68/41
WICHITA FALLS    71  43         SUNNY   80/49   SUNNY   83/47
WILKES BARRE     50  36   .03   PTCLDY  55/40   MOCLDY  51/30
WILMINGTON DE    58  37         PTCLDY  58/44   SUNNY   60/35
YAKIMA           50  43         MOCLDY  56/37   MOCLDY  53/38
YOUNGSTOWN       45  33   .02   PTCLDY  59/38   MOCLDY  51/30
YUMA             88  62         SUNNY   81/57   SUNNY   81/58

NATIONAL TEMPERATURE EXTREMES

HIGH FRI...97 AT MESA (PHOENIX-MESA GATEWAY AIRPORT) AZ

LOW  SAT...21 AT GUNNISON CO AND MINOT AFB ND


$$</pre><br><hr><br><br><center><strong><em>Last Updated: 2010-10-30 12:50:07</em></strong></center><br><hr><br><br>                 </td>
              </tr>
            </table>
            <!--  *****************  END CONTENT SECTION  *************   --></td>
        </tr>
      </table>
      <!-- InstanceEndEditable -->
      <!-- end content area -->
    </td>
  </tr>
  <tr>
    <td valign="top">&nbsp;</td>
    <td><!-- start footer -->
      <!-- start footer inc -->
<table width="98%" border="0" cellpadding="2" cellspacing="0">
  <tr>
    <td colspan="3"><hr align="right" width="98%" />
    </td>
  </tr>
  <tr valign="top">
    <td align="left" nowrap="nowrap" class="gray">
	&#160;&#160;&#160;&#160;<a href="http://www.doc.gov"><span class="gray">US Dept of Commerce</span></a><br />
	&#160;&#160;&#160;&#160;<a href="http://www.noaa.gov"><span class="gray">National Oceanic and Atmospheric Administration</span></a><br />
	&#160;&#160;&#160;&#160;National Weather Service<br />
	&#160;&#160;&#160;&#160;1325 East West Highway<br />
	&#160;&#160;&#160;&#160;Silver Spring, MD 20910<br />
	&#160;&#160;&#160;&#160;Page Author: <a href="mailto:w-nws.webmaster@noaa.gov">
	<span class="gray">NWS Internet Services Team</span></a>
	</td>
    <td nowrap="nowrap"><a href="/disclaimer.php"><span class="gray">Disclaimer</span></a><br />
    <a href="http://www.cio.noaa.gov/Policy_Programs/info_quality.html"><span class="gray">Information Quality</span></a><br />
    <a href="/credits.php"><span class="gray">Credits</span></a><br />
    <a href="/glossary/"><span class="gray">Glossary</span></a><br />
	</td>
    <td align="right" nowrap="nowrap"><a href="/privacy.php"><span class="gray">Privacy Policy</span></a><br />
    <a href="http://www.rdc.noaa.gov/%7Efoia/"><span class="gray">Freedom of Information Act (FOIA)</span></a><br />
    <a href="/admin.php"><span class="gray">About Us</span></a><br />
    <a href="/careers.php"><span class="gray">Career Opportunities</span></a>
	</td>
  </tr>
</table>
<!-- end footer inc -->      <!-- end footer -->
      &nbsp;&nbsp;&nbsp;&nbsp; <span class="gray">Page last Modified:</span> <!-- InstanceBeginEditable name="DateLastMod" --> <span class="gray">
      <!-- start modified date -->
      <!-- #BeginDate format:Sw1a -->12 January, 2009 4:15 PM<!-- #EndDate -->
      <!-- end modified date -->
      </span> <!-- InstanceEndEditable --> </td>
  </tr>
</table>
</body>
<!-- InstanceEnd -->
</html>
