<%@ page
	contentType="text/html;charset=UTF-8"
	language="java"
	import="com.google.appengine.api.users.*,
	        java.net.*,
	        zinger.wbj.server.*"
%>
<%
	UserService userService = UserServiceFactory.getUserService();
	String destUrl = request.getRequestURL().toString();
	
	if(!userService.isUserAdmin())
	{
		response.sendRedirect(userService.createLoginURL(destUrl));
		return;
	}
	
	String subject = request.getParameter(AnnouncementServlet.SUBJECT_PARAM);
	if(subject != null)
		subject = URLDecoder.decode(subject);
	String body = request.getParameter(AnnouncementServlet.BODY_PARAM);
	if(body != null)
		body = URLDecoder.decode(body);
%>
<html>
	<head>
		<title>Weather Black Jack Admin</title>
		<link rel="stylesheet" type="text/css" href="../style.css"/>
	</head>
	<body>
		<form method="POST" action="announcement">
			<table border="0">
				<tr>
					<td>Subject:</td>
					<td><input type="text" name="subject" value="<%= subject %>"></input></td>
				</tr>
				<tr>
					<td>Body:</td>
					<td><textarea rows="25" cols="80" name="body" value="<%= body %>"></textarea></td>
				</tr>
				<tr>
					<td></td>
					<td><input type="submit" value="Send"></input></td>
				</tr>
			</table>
		</form>
	</body>
</html>
