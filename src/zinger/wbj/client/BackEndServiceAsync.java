package zinger.wbj.client;

import com.google.gwt.user.client.rpc.*;

import java.util.*;

public interface BackEndServiceAsync
{
	public void queryLocations(String locationPrefix, AsyncCallback<Set<String>> callback);
	
	public void authenticateUser(String user, String pass, AsyncCallback<Boolean> callback);
	public void createUser(String user, String pass, AsyncCallback<Boolean> callback);
	
	public void getBets(AsyncCallback<List<Wager>> callback);
	public void submitBet(String location, int high, AsyncCallback<Void> callback);
	public void getWinnersByLocation(String lcation, AsyncCallback<Winnings> callback);
	public void getBetLocations(AsyncCallback<Set<String>> callback);
	
	public void getPools(AsyncCallback<Set<String>> callback);
	public void joinPool(String pool, AsyncCallback<Void> callback);
	public void leavePool(String pool, AsyncCallback<Void> callback);
	public void queryPools(String poolQuery, AsyncCallback<Set<String>> callback);
	public void getPoolTally(String pool, AsyncCallback<Float> callback);
	
	public void invite(String invitedEmail, String invitedToPool, AsyncCallback<Void> callback);
	public void getReceivedInvites(AsyncCallback<Set<String>> callback);
	public void dismissInvites(Set<String> invitedToPools, AsyncCallback<Void> callback);
}
