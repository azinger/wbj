package zinger.wbj.client;

import com.google.gwt.core.client.*;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.event.logical.shared.*;
import com.google.gwt.user.client.*;
import com.google.gwt.user.client.rpc.*;
import com.google.gwt.user.client.ui.*;

import java.util.*;

import zinger.wbj.client.ui.*;
import static zinger.wbj.client.Util.*;

public class BettingUi implements EntryPoint
{
	protected static final int RESULTS_COLUMN_COUNT = 3;
	
	protected final SuggestBox locationInput = new SuggestBox(new LocationOracle());
	protected final ValueBoxBase<Integer> highInput = new IntegerBox();
	protected final Button checkWinnersButton = new Button("Check Yesterday's Winners");
	protected final Button submitBetButton = new Button("Submit Bet");
	protected final Button myBetsButton = new Button("My Bets");
	protected final Button myPoolsButton = new Button("My Pools");
	protected final Panel resultsPanel = new VerticalPanel();
	
	public BettingUi()
	{
		locationInput.setAnimationEnabled(true);
		
		checkWinnersButton.addClickHandler(new ClickHandler()
		{
			public void onClick(final ClickEvent ev)
			{
				getWinners();
			}
		});
		
		submitBetButton.addClickHandler(new ClickHandler()
		{
			public void onClick(final ClickEvent ev)
			{
				submitBet();
			}
		});
		
		myBetsButton.addClickHandler(new ClickHandler()
		{
			public void onClick(final ClickEvent ev)
			{
				myBets();
			}
		});
		
		myPoolsButton.addClickHandler(new ClickHandler()
		{
			public void onClick(final ClickEvent ev)
			{
				myPools();
			}
		});
	}
	
	public void onModuleLoad()
	{
		panel(
			RootPanel.get("output"),
			grid(
				new Grid(),
				new Widget[][]
				{
					{ new InlineLabel("City"), locationInput, checkWinnersButton },
					{ new InlineLabel("High"), highInput, submitBetButton }
				}
			),
			panel(
				new HorizontalPanel(),
				myBetsButton,
				myPoolsButton
			),
			resultsPanel
		);
		
		getResults();
		
		receiveInvites();
	}
	
	protected void getResults()
	{
		resultsPanel.clear();
		backEnd().getBetLocations(new LoginCallback<Set<String>>()
		{
			public void onSuccess(final Set<String> locations)
			{
				if(locations.isEmpty())
					return;
				//final int rowCount = locations.size() / RESULTS_COLUMN_COUNT + locations.size() % RESULTS_COLUMN_COUNT > 0 ? 1 : 0;
				final int rowCount = locations.size() / RESULTS_COLUMN_COUNT + 1;
				final Grid winningsPanel = new Grid(rowCount, RESULTS_COLUMN_COUNT);
				panel(
					resultsPanel,
					new VerticalPanel(),
					new InlineLabel("Yesterday's Results:"),
					winningsPanel
				);
				int locationIx = -1;
				for(final String location : locations)
				{
					++locationIx;
					final int rowIx = locationIx / RESULTS_COLUMN_COUNT;
					final int colIx = locationIx % RESULTS_COLUMN_COUNT;
					final boolean checkeredFlagMarker = ((rowIx + colIx) & 1) == 0;
					backEnd().getWinnersByLocation(location, new AsyncCallback<Winnings>()
					{
						public void onSuccess(final Winnings winnings)
						{
							//winningsPanel.add(new WinnersDisplay(winnings));
							winningsPanel.setWidget(
								rowIx,
								colIx,
								css(
									new WinnersDisplay(winnings), 
									checkeredFlagMarker ? "checkeredFlag-TopLeft" : "checkeredFlag-NotTopLeft"
								)
							);
						}
						
						public void onFailure(final Throwable ex)
						{
						}
					});
				}
			}
			
			protected void handleLogin(final InsufficientPrivilegesException ex)
			{
				resultsPanel.add(panel(
					new FlowPanel(),
					new InlineLabel("Please "),
					ex.getLoginUrl() == null ? new InlineLabel("log in") : new Anchor("log in", false, ex.getLoginUrl())
				));
			}
			
			protected void loginSuccess()
			{
				getResults();
			}
		});
	}

	protected void getWinners()
	{
		final String location = this.locationInput.getText();
		backEnd().getWinnersByLocation(location, new AsyncCallback<Winnings>()
		{
			public void onSuccess(final Winnings winnings)
			{
				panel(
					new SimpleDialog("Winners' Circle", true),
					new WinnersDisplay(winnings)
				).showCenter();
			}
			
			public void onFailure(final Throwable ex)
			{
				Window.alert(ex.getMessage());
			}
		});
	}
	
	protected void submitBet()
	{
		final String location = this.locationInput.getText();
		final int high = this.highInput.getValue();
		backEnd().submitBet(location, high, new LoginCallback<Void>()
		{
			public void onSuccess(final Void nada)
			{
				highInput.setText("BET ACCEPTED");
				new com.google.gwt.user.client.Timer()
				{
					public void run()
					{
						locationInput.setText(null);
						highInput.setText(null);
						locationInput.setFocus(true);
					}
				}.schedule(2000);
			}
			
			protected void handleFailure(final Throwable ex)
			{
				if(ex instanceof BetRejectionException)
					Window.alert(ex.getMessage());
				else
					super.handleFailure(ex);
			}
			
			protected void loginSuccess()
			{
				submitBet();
			}
		});
	}
	
	protected void myBets()
	{
		backEnd().getBets(new LoginCallback<List<Wager>>()
		{
			public void onSuccess(final List<Wager> wagers)
			{
				panel(
					new SimpleDialog("My Bets", true),
					new WagersDisplay(wagers)
				).showCenter();
			}
			
			protected void loginSuccess()
			{
				myBets();
			}
		});
	}
	
	protected void myPools()
	{
		panel(
			new SimpleDialog("My Pools", true),
			new PoolUi()
		).showCenter();
	}
	
	protected void receiveInvites()
	{
		backEnd().getReceivedInvites(new AsyncCallback<Set<String>>()
		{
			public void onSuccess(final Set<String> invitedToPools)
			{
				if(invitedToPools == null || invitedToPools.isEmpty())
					return;
				
				final InviteTriageUi inviteTriageUi = new InviteTriageUi(invitedToPools);
				final SimpleDialog dialog = panel(
					new SimpleDialog("My Invites", true),
					inviteTriageUi
				);
				inviteTriageUi.addValueChangeHandler(new ValueChangeHandler<Map<String, Boolean>>()
				{
					public void onValueChange(final ValueChangeEvent<Map<String, Boolean>> ev)
					{
						dialog.hide();
						processInvites(ev.getValue());
					}
				});
				dialog.showCenter();
			}
			
			public void onFailure(final Throwable ex)
			{
			}
		});
	}
	
	protected void processInvites(final Map<String, Boolean> inviteDecisions)
	{
		for(final Map.Entry<String, Boolean> inviteDecision : inviteDecisions.entrySet())
		{
			if(inviteDecision.getValue())
				backEnd().joinPool(inviteDecision.getKey(), VOID_CALLBACK);
		}
		backEnd().dismissInvites(new HashSet<String>(inviteDecisions.keySet()), VOID_CALLBACK);
	}
}
