package zinger.wbj.client;

import com.google.gwt.user.client.rpc.*;

import java.util.*;

public class Winnings implements IsSerializable
{
	private Wager observed;
	private List<Wager> winners;
	private int totalSubmitted;
	
	public Winnings() {}
	
	public Winnings(final Wager observed, final List<Wager> winners, final int totalSubmitted)
	{
		this.observed = observed;
		this.winners = winners;
		this.totalSubmitted = totalSubmitted;
	}
	
	public Wager getObserved() { return this.observed; }
	public List<Wager> getWinners() { return this.winners; }
	public int getTotalSubmitted() { return this.totalSubmitted; }
	
	@Override
	public int hashCode()
	{
		return 
			this.totalSubmitted |
			(this.observed == null ? 0 : this.observed.hashCode()) |
			(this.winners == null ? 0 : this.winners.hashCode());
	}
	
	@Override
	public boolean equals(final Object obj)
	{
		if(!(obj instanceof Winnings))
			return false;
		final Winnings other = (Winnings)obj;
		return 
			this.getTotalSubmitted() == other.getTotalSubmitted() &&
			Util.equal(this.getObserved(), other.getObserved()) &&
			Util.equal(this.getWinners(), other.getWinners());
	}
}
