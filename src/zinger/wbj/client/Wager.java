package zinger.wbj.client;

import com.google.gwt.user.client.rpc.*;

public class Wager implements IsSerializable
{
	private String location;
	private String user;
	private int high;
	
	public Wager() {}
	
	public Wager(final String location, final String user, final int high)
	{
		this.location = location;
		this.user = user;
		this.high = high;
	}
	
	public String getLocation() { return this.location; }
	public String getUser() { return this.user; }
	public int getHigh() { return this.high; }
	
	@Override
	public int hashCode()
	{
		return 
			this.high |
			(this.location == null ? 0 : this.location.hashCode()) |
			(this.user == null ? 0 : this.user.hashCode());
	}
	
	@Override
	public boolean equals(final Object obj)
	{
		if(!(obj instanceof Wager))
			return false;
		final Wager other = (Wager)obj;
		return 
			this.getHigh() == other.getHigh() &&
			Util.equal(this.getLocation(), other.getLocation()) &&
			Util.equal(this.getUser(), other.getUser());
	}
}
