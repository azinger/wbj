package zinger.wbj.client;

import com.google.gwt.user.client.rpc.*;

import java.util.*;

@RemoteServiceRelativePath("back-end")
public interface BackEndService extends RemoteService
{
	public Set<String> queryLocations(String locationPrefix);
	
	public boolean authenticateUser(String user, String pass) throws NotFoundException;
	public boolean createUser(String user, String pass);
	
	public List<Wager> getBets() throws InsufficientPrivilegesException;
	public void submitBet(String location, int high) throws InsufficientPrivilegesException, BetRejectionException;
	public Winnings getWinnersByLocation(String location) throws NotFoundException;
	public Set<String> getBetLocations() throws InsufficientPrivilegesException;
	
	public Set<String> getPools() throws InsufficientPrivilegesException, NotFoundException;
	public void joinPool(String pool) throws InsufficientPrivilegesException, NotFoundException;
	public void leavePool(String pool) throws InsufficientPrivilegesException;
	public Set<String> queryPools(String poolQuery);
	public float getPoolTally(String pool) throws InsufficientPrivilegesException;
	
	public void invite(String invitedEmail, String invitedToPool) throws InsufficientPrivilegesException, NotFoundException;
	public Set<String> getReceivedInvites() throws InsufficientPrivilegesException, NotFoundException;
	public void dismissInvites(Set<String> invitedToPools) throws InsufficientPrivilegesException, NotFoundException;
}
