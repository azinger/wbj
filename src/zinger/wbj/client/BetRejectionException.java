package zinger.wbj.client;

import com.google.gwt.user.client.rpc.*;

public class BetRejectionException extends Exception implements IsSerializable
{
	private BetRejectionException()
	{
	}
	
	public BetRejectionException(final String message)
	{
		super(message);
	}
}
