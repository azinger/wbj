package zinger.wbj.client;

import com.google.gwt.core.client.*;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.event.logical.shared.*;
import com.google.gwt.user.client.*;
import com.google.gwt.user.client.rpc.*;
import com.google.gwt.user.client.ui.*;

import java.util.*;

import zinger.wbj.client.ui.*;
import static zinger.wbj.client.Util.*;


public class LocationOracle extends SuggestOracle
{
	public void requestSuggestions(final SuggestOracle.Request request, final SuggestOracle.Callback callback)
	{
		backEnd().queryLocations(request.getQuery(), new AsyncCallback<Set<String>>()
		{
			public void onSuccess(final Set<String> locations)
			{
				final List<SuggestOracle.Suggestion> suggestions = new ArrayList<SuggestOracle.Suggestion>();
				for(final String location : locations)
				{
					suggestions.add(new SuggestOracle.Suggestion()
					{
						public String getDisplayString() { return location; }
						public String getReplacementString() { return location; }
					});
				}
				final SuggestOracle.Response response = new SuggestOracle.Response();
				response.setSuggestions(suggestions);
				callback.onSuggestionsReady(request, response);
			}

			public void onFailure(final Throwable ex)
			{
				Window.alert(ex.getMessage());
			}
		});
	}
}
