package zinger.wbj.client;

import com.google.gwt.core.client.*;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.event.logical.shared.*;
import com.google.gwt.event.shared.*;
import com.google.gwt.user.client.*;
import com.google.gwt.user.client.rpc.*;
import com.google.gwt.user.client.ui.*;

import java.util.*;

import zinger.wbj.client.ui.*;
import static zinger.wbj.client.Util.*;

public abstract class LoginCallback<T> implements AsyncCallback<T>
{
	protected abstract void loginSuccess();
	
	protected void handleFailure(final Throwable ex)
	{
		Window.alert(ex.getMessage());
	}
	
	/* protected void handleLogin(final InsufficientPrivilegesException ex)
	{
		final Authenticator authenticator = new Authenticator();
		final SimpleDialog dialog = panel(
			new SimpleDialog("Log In/ Create Account", true),
			authenticator
		);
		dialog.showCenter();
		authenticator.addOpenHandler(new OpenHandler<String>()
		{
			public void onOpen(final OpenEvent<String> ev)
			{
				dialog.hide();
				loginSuccess();
			}
		});
	} */
	
	protected void handleLogin(final InsufficientPrivilegesException ex)
	{
		panel(
			new SimpleDialog("Log In", true),
			panel(
				new FlowPanel(),
				new InlineLabel("Please "),
				ex.getLoginUrl() == null ? new InlineLabel("log in") : new Anchor("log in", false, ex.getLoginUrl())
			)
		).showCenter();
	}
	
	public final void onFailure(final Throwable ex)
	{
		if(ex instanceof InsufficientPrivilegesException)
			handleLogin((InsufficientPrivilegesException)ex);
		else
			handleFailure(ex);
	}
}
