package zinger.wbj.client.ui;

import com.google.gwt.core.client.*;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.event.logical.shared.*;
import com.google.gwt.event.shared.*;
import com.google.gwt.user.client.*;
import com.google.gwt.user.client.rpc.*;
import com.google.gwt.user.client.ui.*;

import java.util.*;

import zinger.wbj.client.*;
import static zinger.wbj.client.Util.*;

public class PoolUi extends Composite
{
	protected Grid poolGrid = css(new Grid(), "poolListing");
	protected TextBoxBase poolInput = new TextBox();
	protected Button joinButton = new Button("Join Pool");
	
	public PoolUi()
	{
		joinButton.addClickHandler(new ClickHandler()
		{
			public void onClick(final ClickEvent ev) { joinPool(); }
		});
		
		this.initWidget(
			panel(
				new VerticalPanel(),
				grid(
					poolGrid,
					new Widget[][] { { new InlineLabel("...") } }
				),
				panel(
					new HorizontalPanel(),
					poolInput,
					joinButton
				)
			)
		);
		
		getPools();
	}
	
	protected void showPoolListing(final String pool)
	{
		final Anchor leavePoolButton = new Anchor("leave");
		final Anchor sendInviteButton = new Anchor("invite");
		final Label poolTallyLabel = new InlineLabel();
		
		final int rowIx = poolGrid.getRowCount();
		poolGrid.resize(rowIx + 1, 4);
		poolGrid.setWidget(rowIx, 0, css(new InlineLabel(pool), "poolListingLabel"));
		poolGrid.setWidget(rowIx, 1, css(poolTallyLabel, "poolTallyLabel"));
		poolGrid.setWidget(rowIx, 2, css(leavePoolButton, "poolListingDelete"));
		poolGrid.setWidget(rowIx, 3, css(sendInviteButton, "poolListingInvite"));
		
		leavePoolButton.addClickHandler(new ClickHandler()
		{
			public void onClick(final ClickEvent ev)
			{
				if(Window.confirm("You are about to leave pool " + pool))
				{
					for(int rowIx = poolGrid.getRowCount() - 1; rowIx >= 0; --rowIx)
					{
						if(poolGrid.getWidget(rowIx, 3) == leavePoolButton)
						{
							poolGrid.removeRow(rowIx);
							break;
						}
					}
					leavePool(pool);
				}
			}
		});
		
		sendInviteButton.addClickHandler(new ClickHandler()
		{
			public void onClick(final ClickEvent ev)
			{
				final String invitedEmail = Window.prompt("Who would you like to invite to pool " + pool + "?", null);
				if(invitedEmail != null)
					sendInvite(invitedEmail, pool);
			}
		});
		
		poolTallyLabel.setText("...");
		backEnd().getPoolTally(pool, new AsyncCallback<Float>()
		{
			public void onSuccess(final Float poolTally)
			{
				if(poolTally != null && poolTally.floatValue() != 0F)
					poolTallyLabel.setText(poolTally.toString());
				else
					poolTallyLabel.setText("");
			}
			
			public void onFailure(final Throwable ex)
			{
				// not much we can do
			}
		});
	}
	
	protected void getPools()
	{
		backEnd().getPools(new LoginCallback<Set<String>>()
		{
			public void onSuccess(final Set<String> pools)
			{
				poolGrid.resize(0, 0);
				for(final String pool : new TreeSet<String>(pools))
					showPoolListing(pool);
			}
			
			protected void loginSuccess()
			{
				getPools();
			}
		});
	}
	
	protected void joinPool()
	{
		final String pool = poolInput.getText();
		if(pool == null || pool.length() == 0)
			return;
		this.showPoolListing(pool);
		backEnd().joinPool(pool, new LoginCallback<Void>()
		{
			protected void loginSuccess()
			{
				joinPool();
			}
			
			protected void handleFailure(final Throwable ex)
			{
				super.handleFailure(ex);
				getPools();
			}
			
			public void onSuccess(final Void nada)
			{
				getPools();
			}
		});
	}
	
	protected void leavePool(final String pool)
	{
		backEnd().leavePool(pool, new LoginCallback<Void>()
		{
			protected void loginSuccess()
			{
				leavePool(pool);
			}
			
			protected void handleFailure(final Throwable ex)
			{
				super.handleFailure(ex);
				getPools();
			}
			
			public void onSuccess(final Void nada)
			{
				getPools();
			}
		});
	}
	
	protected void sendInvite(final String invitedEmail, final String invitedToPool)
	{
		backEnd().invite(invitedEmail, invitedToPool, VOID_CALLBACK);
	}
}
