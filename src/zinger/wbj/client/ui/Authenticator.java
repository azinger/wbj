package zinger.wbj.client.ui;

import com.google.gwt.core.client.*;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.event.logical.shared.*;
import com.google.gwt.event.shared.*;
import com.google.gwt.user.client.*;
import com.google.gwt.user.client.rpc.*;
import com.google.gwt.user.client.ui.*;

import java.util.*;

import zinger.wbj.client.*;
import static zinger.wbj.client.Util.*;

public class Authenticator extends Composite implements HasOpenHandlers<String>
{
	protected final TextBoxBase userInput = new TextBox();
	protected final TextBoxBase passInput = new PasswordTextBox();
	protected final Button authenticateButton;
	
	public Authenticator()
	{
		this("Log In");
	}
	
	public Authenticator(final String authenticateButtonText)
	{
		this.authenticateButton = new Button(authenticateButtonText);
		this.authenticateButton.addClickHandler(new ClickHandler()
		{
			public void onClick(final ClickEvent ev)
			{
				Authenticator.this.authenticate();
			}
		});
		
		this.initWidget(
			grid(
				new Grid(),
				new Widget[][]
				{
					{ new InlineLabel("User"), this.userInput },
					{ new InlineLabel("Password"), this.passInput },
					{ null, this.authenticateButton }
				}
			)
		);
	}
	
	public HandlerRegistration addOpenHandler(final OpenHandler<String> handler)
	{
		return this.addHandler(handler, OpenEvent.getType());
	}
	
	protected void authenticate()
	{
		final String user = this.userInput.getText();
		final String pass = this.passInput.getText();
		backEnd().authenticateUser(user, pass, new AsyncCallback<Boolean>()
		{
			public void onSuccess(final Boolean authenticated)
			{
				if(authenticated)
					OpenEvent.fire(Authenticator.this, user);
				else
					Window.alert("Bad user name or password.");
			}
			
			public void onFailure(final Throwable ex)
			{
				if(ex instanceof NotFoundException)
					Authenticator.this.create(user, pass);
				else
					Window.alert(ex.getMessage());
			}
		});
	}
	
	protected void create(final String user, final String pass)
	{
		final TextBoxBase passwdConfirmInput = new PasswordTextBox();
		final Button confirmPasswdButton = new Button("Confirm Password");
		
		final DialogBox dialog = panel(
			new DialogBox(true),
			panel(
				new HorizontalPanel(),
				passwdConfirmInput,
				confirmPasswdButton
			)
		);
		dialog.setText("Confirm Password");
		dialog.setAnimationEnabled(true);
		
		confirmPasswdButton.addClickHandler(new ClickHandler()
		{
			public void onClick(final ClickEvent ev)
			{
				if(equal(pass, passwdConfirmInput.getText()))
				{
					backEnd().createUser(user, pass, new AsyncCallback<Boolean>()
					{
						public void onSuccess(final Boolean created)
						{
							if(created)
								OpenEvent.fire(Authenticator.this, user);
						}

						public void onFailure(final Throwable ex)
						{
							Window.alert(ex.getMessage());
						}
					});
				}
				dialog.hide();
			}
		});
		
		dialog.center();
		dialog.show();
	}
}
