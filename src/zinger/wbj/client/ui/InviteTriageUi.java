package zinger.wbj.client.ui;

import com.google.gwt.core.client.*;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.event.logical.shared.*;
import com.google.gwt.event.shared.*;
import com.google.gwt.user.client.*;
import com.google.gwt.user.client.rpc.*;
import com.google.gwt.user.client.ui.*;

import java.util.*;

import zinger.wbj.client.*;
import static zinger.wbj.client.Util.*;

public class InviteTriageUi extends Composite implements HasValueChangeHandlers<Map<String, Boolean>>
{
	protected final Map<String, Boolean> inviteDecisions = new HashMap<String, Boolean>();
	
	public InviteTriageUi(final Set<String> invitedToPools)
	{
		final Grid grid = new Grid(invitedToPools.size(), 3);
		int row = -1;
		for(final String invitedToPool : invitedToPools)
		{
			++row;
			
			grid.setText(row, 0, invitedToPool);
			
			final RadioButton acceptRadio = new RadioButton(invitedToPool, "accept");
			acceptRadio.addValueChangeHandler(new ValueChangeHandler<Boolean>()
			{
				public void onValueChange(final ValueChangeEvent<Boolean> ev)
				{
					inviteDecisions.put(invitedToPool, true);
				}
			});
			grid.setWidget(row, 1, acceptRadio);
			
			final RadioButton declineRadio = new RadioButton(invitedToPool, "decline");
			declineRadio.addValueChangeHandler(new ValueChangeHandler<Boolean>()
			{
				public void onValueChange(final ValueChangeEvent<Boolean> ev)
				{
					inviteDecisions.put(invitedToPool, false);
				}
			});
			grid.setWidget(row, 2, declineRadio);
		}
		
		final Button okButton = new Button("OK");
		okButton.addClickHandler(new ClickHandler()
		{
			public void onClick(final ClickEvent ev)
			{
				ValueChangeEvent.fire(InviteTriageUi.this, inviteDecisions);
			}
		});
		
		initWidget(panel(
			new VerticalPanel(),
			grid,
			panel(
				new FlowPanel(),
				okButton
			)
		));
	}
	
	public HandlerRegistration addValueChangeHandler(final ValueChangeHandler<Map<String, Boolean>> handler)
	{
		return addHandler(handler, ValueChangeEvent.getType());
	}
}
