package zinger.wbj.client.ui;

import com.google.gwt.core.client.*;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.event.logical.shared.*;
import com.google.gwt.event.shared.*;
import com.google.gwt.user.client.*;
import com.google.gwt.user.client.rpc.*;
import com.google.gwt.user.client.ui.*;

import java.util.*;

import zinger.wbj.client.*;
import static zinger.wbj.client.Util.*;

public class WagersDisplay extends Composite
{
	public WagersDisplay(final List<Wager> wagers)
	{
		final Grid grid = new Grid(wagers.size(), 2);
		
		int row = -1;
		for(final Wager wager : wagers)
		{
			++row;
			grid.setText(row, 0, wager.getLocation());
			grid.setText(row, 1, String.valueOf(wager.getHigh()));
		}
		
		this.initWidget(grid);
	}
}
