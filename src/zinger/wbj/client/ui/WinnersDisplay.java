package zinger.wbj.client.ui;

import com.google.gwt.core.client.*;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.event.logical.shared.*;
import com.google.gwt.event.shared.*;
import com.google.gwt.user.client.*;
import com.google.gwt.user.client.rpc.*;
import com.google.gwt.user.client.ui.*;

import java.util.*;

import zinger.wbj.client.*;
import static zinger.wbj.client.Util.*;

public class WinnersDisplay extends Composite
{
	public WinnersDisplay(final Winnings winnings)
	{
		final Grid grid = new Grid(winnings.getWinners().size() + 4, 2);
		int row = -1;
		
		++row;
		/* grid.setText(row, 0, "Location");
		grid.setText(row, 1, winnings.getObserved().getLocation()); */
		grid.setWidget(row, 0, css(new InlineLabel(winnings.getObserved().getLocation()), "locationTitle"));
		
		++row;
		grid.setText(row, 0, "Total Bets");
		grid.setText(row, 1, String.valueOf(winnings.getTotalSubmitted()));
		
		++row;
		grid.setText(row, 0, "Observed High");
		grid.setText(row, 1, String.valueOf(winnings.getObserved().getHigh()));
		
		++row;
		if(winnings.getWinners().isEmpty())
			grid.setText(row, 1, "No Winners");
		else
		{
			grid.setText(row, 0, "Winning Bet");
			grid.setText(row, 1, String.valueOf(winnings.getWinners().get(0).getHigh()));
			
			for(final Wager winner : winnings.getWinners())
				grid.setText(++row, 1, winner.getUser());
		}
		
		this.initWidget(grid);
	}
}
