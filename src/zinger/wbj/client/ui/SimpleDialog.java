package zinger.wbj.client.ui;

import com.google.gwt.core.client.*;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.event.logical.shared.*;
import com.google.gwt.event.shared.*;
import com.google.gwt.user.client.*;
import com.google.gwt.user.client.rpc.*;
import com.google.gwt.user.client.ui.*;

import java.util.*;

import zinger.wbj.client.*;
import static zinger.wbj.client.Util.*;

public class SimpleDialog extends DialogBox
{
	public SimpleDialog(final String title, final boolean modal)
	{
		super(modal);
		this.setText(title);
		this.setAnimationEnabled(true);
	}
	
	public void showCenter()
	{
		this.center();
		this.show();
	}
}
