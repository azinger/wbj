package zinger.wbj.server;

import com.google.appengine.api.taskqueue.*;
import com.google.common.base.*;
import com.google.common.collect.*;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.logging.*;

import javax.mail.*;
import javax.mail.internet.*;
import javax.servlet.*;
import javax.servlet.http.*;

import zinger.wbj.client.*;
import zinger.wbj.db.*;
import zinger.wbj.lookup.*;

public class AnnouncementServlet extends HttpServlet
{
	public static final String SUBJECT_PARAM = "subject";
	public static final String BODY_PARAM = "body";
	
	protected final Logger log = Logger.getLogger(this.getClass().getName());
	
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		final String subject = param(request, SUBJECT_PARAM);
		final String body = param(request, BODY_PARAM);
		
		QueueFactory.getDefaultQueue().add(TaskOptions.Builder
			.withUrl("/job/send_announcement")
			.method(TaskOptions.Method.GET)
			.param(SUBJECT_PARAM, subject)
			.param(BODY_PARAM, body)
		);
		
		response.sendRedirect("index.jsp");
	}
	
	protected static String param(final HttpServletRequest request, final String param)
	{
		String val = request.getParameter(param);
		if(val != null)
			val = URLDecoder.decode(val);
		return val;
	}
}
