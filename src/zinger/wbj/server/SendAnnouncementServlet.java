package zinger.wbj.server;

import com.google.appengine.api.taskqueue.*;
import com.google.common.base.*;
import com.google.common.collect.*;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.logging.*;

import javax.mail.*;
import javax.mail.internet.*;
import javax.servlet.*;
import javax.servlet.http.*;

import zinger.wbj.client.*;
import zinger.wbj.db.*;
import zinger.wbj.lookup.*;

import static zinger.wbj.server.AnnouncementServlet.*;
import static zinger.wbj.server.DayRollServlet.*;

public class SendAnnouncementServlet extends HttpServlet
{
	public static final String TO_PARAM = "to";
	
	protected final Logger log = Logger.getLogger(this.getClass().getName());
	
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		final String subject = param(request, SUBJECT_PARAM);
		final String body = param(request, BODY_PARAM);
		final String to = param(request, TO_PARAM);
		
		if(to == null)
			scheduleAll(subject, body);
		else
			send(to, subject, body);
	}
	
	public static void schedule(final String email, final String subject, final String body)
	{
		QueueFactory.getDefaultQueue().add(TaskOptions.Builder
			.withUrl("/job/send_announcement")
			.method(TaskOptions.Method.GET)
			.param(SUBJECT_PARAM, subject)
			.param(BODY_PARAM, body)
			.param(TO_PARAM, email)
		);
	}
	
	protected void scheduleAll(final String subject, final String body) throws IOException
	{
		log.fine(String.format("Scheduling announcement %s: %s", subject, body));
		final com.google.appengine.api.taskqueue.Queue queue = QueueFactory.getDefaultQueue();
		for(final String email : StateManagerFactory.INSTANCE.getStateManager().getUserEmails())
			schedule(email, subject, body);
	}
	
	protected void send(final String to, final String subject, final String body) throws IOException
	{
		log.fine(String.format("Sending announcement %s to %s", subject, to));
		try
		{
			final Session mailSession = Session.getDefaultInstance(new Properties());
			final Message message = new MimeMessage(mailSession);
			message.setSentDate(new Date());
			message.setFrom(new InternetAddress(FROM_EMAIL, FROM_NAME));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			message.setSubject(subject);
			message.setText(body);
			Transport.send(message);
		}
		catch(final MessagingException ex)
		{
			ex.printStackTrace();
		}
	}
}
