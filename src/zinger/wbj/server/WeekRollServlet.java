package zinger.wbj.server;

import com.google.appengine.api.taskqueue.*;

import com.google.common.base.*;
import com.google.common.collect.*;

import java.io.*;
import java.util.*;
import java.util.logging.*;

import javax.mail.*;
import javax.mail.internet.*;
import javax.servlet.*;
import javax.servlet.http.*;

import zinger.wbj.client.*;
import zinger.wbj.db.*;
import zinger.wbj.lookup.*;

public class WeekRollServlet extends HttpServlet
{
	protected final Logger logger = Logger.getLogger(this.getClass().getName());
	
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException
	{
		final StateManager stateManager = StateManagerFactory.INSTANCE.getStateManager();
		final Session mailSession = Session.getDefaultInstance(new Properties());
		
		for(final String pool : stateManager.getAllPools())
			this.rollPool(stateManager, mailSession, pool);
	}
	
	protected void rollPool(final StateManager stateManager, final Session mailSession, final String pool)
	{
		for(final String user : stateManager.getUsersInPool(pool))
			this.rollUserPool(stateManager, mailSession, pool, user);
	}
	
	protected void rollUserPool(final StateManager stateManager, final Session mailSession, final String pool, final String user)
	{
		try
		{
			this.sendNotification(stateManager, mailSession, user, pool, stateManager.getUserTally(user, pool));
		}
		catch(final MessagingException ex)
		{
			ex.printStackTrace();
		}
		catch(final UnsupportedEncodingException ex)
		{
			ex.printStackTrace();
		}
		catch(final NotFoundException ex)
		{
			ex.printStackTrace();
		}
		
		stateManager.clearUserTallies(user, pool);
	}
	
	protected void sendNotification(
		final StateManager stateManager, 
		final Session mailSession, 
		final String user, 
		final String pool, 
		final float tally
	) throws MessagingException, UnsupportedEncodingException, NotFoundException
	{
		final String smiley;
		if(tally > 0F)
			smiley = ":)";
		else if(tally < 0F)
			smiley = ":(";
		else smiley = ":|";
		
		final String userEmail = stateManager.getUserEmail(user);
		final Message message = new MimeMessage(mailSession);
		message.setSentDate(new Date());
		message.setFrom(new InternetAddress(DayRollServlet.FROM_EMAIL, DayRollServlet.FROM_NAME));
		message.addRecipient(Message.RecipientType.TO, new InternetAddress(userEmail, user));
		message.setSubject(String.format("Your week's tally in %s", pool));
		message.setText(String.format(
			"Dear %s,\n" +
			"Your week's tally in pool %s is %.2f %s",
			user, 
			pool, 
			tally,
			smiley
		));
		Transport.send(message);
	}
}
