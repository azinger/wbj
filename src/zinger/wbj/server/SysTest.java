package zinger.wbj.server;

import java.io.*;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;

public class SysTest extends HttpServlet
{
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException
	{
		PrintWriter w = null;
		try
		{
			w = new PrintWriter(new OutputStreamWriter(response.getOutputStream()));
			
			final Map<Object, Object> props = new TreeMap<Object, Object>();
			props.putAll(System.getProperties());
			for(final Map.Entry<?, ?> entry : props.entrySet())
				w.println(String.format("%s\t%s", entry.getKey(), entry.getValue()));
			
			w.flush();
		}
		catch(final IOException ex)
		{
			throw new ServletException(ex);
		}
	}
}
