package zinger.wbj.server;

//import com.google.gwt.rpc.server.*;
import com.google.gwt.user.server.rpc.*;

import com.google.appengine.api.taskqueue.*;
import com.google.appengine.api.users.*;

import java.util.*;
import java.util.logging.*;

import zinger.wbj.client.*;
import zinger.wbj.db.*;
import zinger.wbj.lookup.*;

import static zinger.wbj.client.Util.*;

public class BackEndServiceImpl extends RemoteServiceServlet implements BackEndService
{
	protected final Logger log = Logger.getLogger(this.getClass().getName());
	protected final UserService userService = UserServiceFactory.getUserService();
	
	public Set<String> queryLocations(final String locationPrefix)
	{
		log.info("Querying " + locationPrefix);
		return new HashSet<String>(LookupManager.INSTANCE.getLookup().queryLocation(locationPrefix));
	}
	
	public boolean authenticateUser(final String user, final String pass) throws NotFoundException
	{
		final boolean authenticated = StateManagerFactory.INSTANCE.getStateManager().getUserPassHash(user) == pass.hashCode();
		/* if(authenticated)
			this.setLoggedInUser(user); */
		return authenticated;
	}
	
	public boolean createUser(final String user, final String pass)
	{
		validStrings(user, pass);
		final StateManager stateManager = StateManagerFactory.INSTANCE.getStateManager();
		try
		{
			stateManager.getUserPassHash(user);
			return false;
		}
		catch(final NotFoundException ex)
		{
			final Map<String, Integer> passHashMap = new HashMap<String, Integer>();
			passHashMap.put(user, pass.hashCode());
			stateManager.setUserPassHashMulti(passHashMap);
			//this.setLoggedInUser(user);
			return true;
		}
	}
	
	public List<Wager> getBets() throws InsufficientPrivilegesException
	{
		final String user = this.getLoggedInUser();
		final Map<String, Number> bets = StateManagerFactory.INSTANCE.getStateManager().getBetsByUser(user);
		final List<Wager> wagers = new ArrayList<Wager>(bets.size());
		for(final Map.Entry<String, Number> entry : bets.entrySet())
			wagers.add(new Wager(entry.getKey(), user, entry.getValue().intValue()));
		return wagers;
	}
	
	public void submitBet(String location, final int high) throws InsufficientPrivilegesException, BetRejectionException
	{
		location = location.toUpperCase();
		validStrings(location);
		final Calendar cal = Calendar.getInstance(LocationTimeZone.INSTANCE.forLocation(location));
		final int hour = cal.get(Calendar.HOUR_OF_DAY);
		final int minute = cal.get(Calendar.MINUTE);
		if(hour >= 11 || hour == 10 && minute >= 30)
			throw new BetRejectionException("Sorry, friend.  You missed today's deadline for this location.");
		final String user = this.getLoggedInUser();
		StateManagerFactory.INSTANCE.getStateManager().setUserEmail(user, userService.getCurrentUser().getEmail());
		if(!StateManagerFactory.INSTANCE.getStateManager().submitBet(user, location, high))
			throw new BetRejectionException("You've already bet in this location today.  Wanna bet elswhere?");
		
		final com.google.appengine.api.taskqueue.Queue taskQueue = QueueFactory.getDefaultQueue();
		taskQueue.add(TaskOptions.Builder
			.withUrl("/job/notify_pool_bet")
			.method(TaskOptions.Method.GET)
			.param(TallyPoolServlet.LOCATION_PARAM, location)
			.param(NotifyPoolBetServlet.USER_PARAM, user)
		);
	}
	
	public Winnings getWinnersByLocation(final String location) throws NotFoundException
	{
		final Map<String, Number> bets = StateManagerFactory.INSTANCE.getStateManager().getBetsByLocation(location);
		final List<Wager> wagers = new ArrayList<Wager>(bets.size());
		for(final Map.Entry<String, Number> entry : bets.entrySet())
			wagers.add(new Wager(location, entry.getKey(), entry.getValue().intValue()));
		
		final int observedHigh = LookupManager.INSTANCE.getLookup().get(location);
		
		final List<Wager> winners = Util.getWinners(observedHigh, wagers);
		
		return new Winnings(new Wager(location, null, observedHigh), winners, wagers.size());
	}
	
	public Set<String> getBetLocations() throws InsufficientPrivilegesException
	{
		final String user = this.getLoggedInUser();
		return StateManagerFactory.INSTANCE.getStateManager().getBetLocationsByUser(user);
	}
	
	/* protected String getLoggedInUser() throws InsufficientPrivilegesException
	{
		final String user = (String)this.getThreadLocalRequest().getSession().getAttribute("login");
		if(user == null)
			throw new InsufficientPrivilegesException();
		return user;
	}
	
	protected void setLoggedInUser(final String user)
	{
		this.getThreadLocalRequest().getSession().setAttribute("login", user);
	} */
	
	protected String getLoggedInUser() throws InsufficientPrivilegesException
	{
		final User user = userService.getCurrentUser();
		log.info(String.format("User: %s", user));
		if(user == null)
			throw new InsufficientPrivilegesException("Not logged in", userService.createLoginURL("/"));
		return user.getNickname();
	}
	
	/* public void rollDay()
	{
		LookupManager.INSTANCE.getLookup().rollDay();
	} */
	
	public Set<String> getPools() throws InsufficientPrivilegesException, NotFoundException
	{
		return StateManagerFactory.INSTANCE.getStateManager().getUserPools(this.getLoggedInUser());
	}
	
	public void joinPool(final String pool) throws InsufficientPrivilegesException, NotFoundException
	{
		validStrings(pool);
		StateManagerFactory.INSTANCE.getStateManager().addPool(this.getLoggedInUser(), pool);
	}
	
	public void leavePool(final String pool) throws InsufficientPrivilegesException
	{
		StateManagerFactory.INSTANCE.getStateManager().removePool(this.getLoggedInUser(), pool);
	}
	
	public Set<String> queryPools(final String poolQuery)
	{
		return StateManagerFactory.INSTANCE.getStateManager().queryPools(poolQuery);
	}
	
	public float getPoolTally(final String pool) throws InsufficientPrivilegesException
	{
		return StateManagerFactory.INSTANCE.getStateManager().getUserTally(this.getLoggedInUser(), pool);
	}
	
	public void invite(final String invitedEmail, final String invitedToPool) throws InsufficientPrivilegesException, NotFoundException
	{
		if(!getPools().contains(invitedToPool))
			throw new InsufficientPrivilegesException(String.format("You must be a member of pool %s before inviting others to it.", invitedToPool));
		
		try
		{
			StateManagerFactory.INSTANCE.getStateManager().addInvite(
				this.getLoggedInUser(),
				invitedEmail,
				invitedToPool
			);
		}
		catch(final IllegalArgumentException ex)
		{
			throw new InsufficientPrivilegesException(ex.getMessage());
		}
		
		final String body = String.format(
			"Dear weather enthusiast,\n\n" +
			"You are hereby invited by a fellow player to participate in a meteorological contest " +
			"courtesy of Weather Black Jack in player pool %s.  " +
			"Simply log in at your convenience at\n\n" + 
			"http://weatherblackjack.appspot.com\n\n" +
			"and follow instructions.  Do refer to the help section for more information.  Happy playing!\n\n" +
			"Sincerely,\n" +
			"Your friendly staff at Weather Black Jack",
			invitedToPool
		);
		
		SendAnnouncementServlet.schedule(invitedEmail, "Invitation to Weather Black Jack pool", body);
	}
	
	public Set<String> getReceivedInvites() throws InsufficientPrivilegesException, NotFoundException
	{
		final String user = this.getLoggedInUser();
		final StateManager stateManager = StateManagerFactory.INSTANCE.getStateManager();
		final String email = stateManager.getUserEmail(user);
		final Set<String> receivedInvites = stateManager.getInvitesForEmail(email);
		if(receivedInvites != null && !receivedInvites.isEmpty())
			log.info(String.format("%s (%s) received invites  to %s", user, email, receivedInvites));
		return receivedInvites;
	}
	
	public void dismissInvites(final Set<String> invitedToPools) throws InsufficientPrivilegesException, NotFoundException
	{
		if(invitedToPools == null && invitedToPools.isEmpty())
			return;
		final String user = this.getLoggedInUser();
		final StateManager stateManager = StateManagerFactory.INSTANCE.getStateManager();
		final String email = stateManager.getUserEmail(user);
		stateManager.dismissInvites(email, invitedToPools);
		log.info(String.format("%s (%s) dismissed invites to %s", user, email, invitedToPools));
	}
	
	@Override
	public String toString() { return "back-end"; }
}
