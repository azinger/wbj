package zinger.wbj.server;

import com.google.common.base.*;
import com.google.common.collect.*;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.logging.*;

import javax.mail.*;
import javax.mail.internet.*;
import javax.servlet.*;
import javax.servlet.http.*;

import zinger.wbj.client.*;
import zinger.wbj.db.*;
import zinger.wbj.lookup.*;

public class NotifyPoolBetServlet extends HttpServlet
{
	public static final String USER_PARAM = "user";
	
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		final String location = URLDecoder.decode(request.getParameter(TallyPoolServlet.LOCATION_PARAM));
		final String originalUser = URLDecoder.decode(request.getParameter(NotifyPoolBetServlet.USER_PARAM));
		
		if(location == null || originalUser == null)
		{
			System.err.println(String.format("Missing required param: location: %s; originalUser: %s", location, originalUser));
			return;
		}
		
		final StateManager stateManager = StateManagerFactory.INSTANCE.getStateManager();
		final Session mailSession = Session.getDefaultInstance(new Properties());
		
		final Set<String> usersWithBets = stateManager.getBetsByLocation(location, true).keySet();
		System.out.println("Users with bets: " + usersWithBets);
		
		final SetMultimap<String, String> usersByPool = HashMultimap.create();
		try
		{
			for(final String pool : stateManager.getUserPools(originalUser))
			{
				final Set<String> usersInPool = stateManager.getUsersInPool(pool);
				usersInPool.remove(originalUser);
				if(!Sets.intersection(usersWithBets, usersInPool).isEmpty())
					continue;
				usersByPool.putAll(pool, usersInPool);
			}
		}
		catch(final NotFoundException ex)
		{
			ex.printStackTrace();
			throw new ServletException(ex);
		}
		
		for(final String pool : usersByPool.keySet())
		{
			for(final String user : usersByPool.get(pool))
			{
				try
				{
					notifyOfBet(user, pool, location, mailSession, stateManager);
				}
				catch(final MessagingException ex)
				{
					ex.printStackTrace();
				}
				catch(final UnsupportedEncodingException ex)
				{
					ex.printStackTrace();
				}
				catch(final NotFoundException ex)
				{
					ex.printStackTrace();
				}
			}
		}
	}
	
	protected void notifyOfBet(final String user, final String pool, final String location, final Session mailSession, final StateManager stateManager) 
		throws MessagingException, UnsupportedEncodingException, NotFoundException
	{
		final Address to = new InternetAddress(stateManager.getUserEmail(user), user);
		System.out.println(String.format("Notifying %s of bet in %s.", to, location));
		final Message message = new MimeMessage(mailSession);
		message.setSentDate(new Date());
		message.setFrom(new InternetAddress(DayRollServlet.FROM_EMAIL, DayRollServlet.FROM_NAME));
		message.addRecipient(Message.RecipientType.TO, to);
		message.setSubject(String.format("Someone has bet in one of your pools (%s)", pool, location));
		message.setText(String.format(
			"Dear %s,\n" +
			"You might be interested to know that there is activity in pool %s in %s.\n" +
			"Visit http://weatherblackjack.appspot.com and get in on the action!",
			user,
			pool,
			location
		));
		Transport.send(message);
	}
}
