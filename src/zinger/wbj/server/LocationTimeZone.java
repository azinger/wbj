package zinger.wbj.server;

import java.util.*;

public class LocationTimeZone
{
	public static final LocationTimeZone INSTANCE = new LocationTimeZone();
	
	protected final Map<String, TimeZone> timeZoneDefs;
	protected final TimeZone defaultTimeZone;
	
	protected LocationTimeZone()
	{
		final TimeZone easternTime = TimeZone.getTimeZone("America/New_York");
		final TimeZone centralTime = TimeZone.getTimeZone("America/Chicago");
		final TimeZone mountainTime = TimeZone.getTimeZone("America/Denver");
		final TimeZone pacificTime = TimeZone.getTimeZone("America/Los_Angeles");
		final TimeZone alaskaTime = TimeZone.getTimeZone("America/Anchorage");
		final TimeZone hawaiiTime = TimeZone.getTimeZone("America/Adak");
		
		final Map<String, TimeZone> stateTimeZone = new HashMap<String, TimeZone>();
		stateTimeZone.put("AK", hawaiiTime);
		stateTimeZone.put("HI", hawaiiTime);
		stateTimeZone.put("CT", easternTime);
		stateTimeZone.put("DC", easternTime);
		stateTimeZone.put("DE", easternTime);
		stateTimeZone.put("FL", easternTime);
		stateTimeZone.put("GA", easternTime);
		stateTimeZone.put("IN", easternTime);
		stateTimeZone.put("KY", easternTime);
		stateTimeZone.put("MA", easternTime);
		stateTimeZone.put("MD", easternTime);
		stateTimeZone.put("ME", easternTime);
		stateTimeZone.put("MI", easternTime);
		stateTimeZone.put("NC", easternTime);
		stateTimeZone.put("NH", easternTime);
		stateTimeZone.put("NJ", easternTime);
		stateTimeZone.put("NY", easternTime);
		stateTimeZone.put("OH", easternTime);
		stateTimeZone.put("PA", easternTime);
		stateTimeZone.put("RI", easternTime);
		stateTimeZone.put("SC", easternTime);
		stateTimeZone.put("TN", easternTime);
		stateTimeZone.put("VA", easternTime);
		stateTimeZone.put("VT", easternTime);
		stateTimeZone.put("WV", easternTime);
		stateTimeZone.put("AL", centralTime);
		stateTimeZone.put("AR", centralTime);
		stateTimeZone.put("FL", centralTime);
		stateTimeZone.put("IA", centralTime);
		stateTimeZone.put("IL", centralTime);
		stateTimeZone.put("IN", centralTime);
		stateTimeZone.put("KS", centralTime);
		stateTimeZone.put("KY", centralTime);
		stateTimeZone.put("LA", centralTime);
		stateTimeZone.put("MI", centralTime);
		stateTimeZone.put("MN", centralTime);
		stateTimeZone.put("MO", centralTime);
		stateTimeZone.put("MS", centralTime);
		stateTimeZone.put("ND", centralTime);
		stateTimeZone.put("NE", centralTime);
		stateTimeZone.put("OK", centralTime);
		stateTimeZone.put("SD", centralTime);
		stateTimeZone.put("TN", centralTime);
		stateTimeZone.put("TX", centralTime);
		stateTimeZone.put("WI", centralTime);
		stateTimeZone.put("AZ", mountainTime);
		stateTimeZone.put("AZ", mountainTime);
		stateTimeZone.put("CO", mountainTime);
		stateTimeZone.put("ID", mountainTime);
		stateTimeZone.put("KS", mountainTime);
		stateTimeZone.put("MT", mountainTime);
		stateTimeZone.put("ND", mountainTime);
		stateTimeZone.put("NE", mountainTime);
		stateTimeZone.put("NM", mountainTime);
		stateTimeZone.put("OR", mountainTime);
		stateTimeZone.put("SD", mountainTime);
		stateTimeZone.put("TX", mountainTime);
		stateTimeZone.put("UT", mountainTime);
		stateTimeZone.put("WY", mountainTime);
		stateTimeZone.put("CA", pacificTime);
		stateTimeZone.put("ID", pacificTime);
		stateTimeZone.put("NV", pacificTime);
		stateTimeZone.put("OR", pacificTime);
		stateTimeZone.put("WA", pacificTime);
		stateTimeZone.put("AK", alaskaTime);
		
		final Map<String, String> locationState = new HashMap<String, String>();
		locationState.put("ANCHORAGE", "AK");
		locationState.put("FAIRBANKS", "AK");
		locationState.put("JUNEAU", "AK");
		locationState.put("BIRMINGHAM", "AL");
		locationState.put("HUNTSVILLE AL", "AL");
		locationState.put("MONTGOMERY", "AL");
		locationState.put("ELKINS", "AR");
		locationState.put("FORT SMITH", "AR");
		locationState.put("LITTLE ROCK", "AR");
		locationState.put("FLAGSTAFF", "AZ");
		locationState.put("PHOENIX", "AZ");
		locationState.put("TUCSON", "AZ");
		locationState.put("YUMA", "AZ");
		locationState.put("FRESNO", "CA");
		locationState.put("LAS VEGAS", "CA");
		locationState.put("LOS ANGELES", "CA");
		locationState.put("SACRAMENTO", "CA");
		locationState.put("SAN DIEGO", "CA");
		locationState.put("SAN FRANCISCO", "CA");
		locationState.put("SAN JOSE", "CA");
		locationState.put("COLORADO SPGS", "CO");
		locationState.put("DENVER", "CO");
		locationState.put("GRAND JUNCTION", "CO");
		locationState.put("PUEBLO", "CO");
		locationState.put("BRIDGEPORT", "CT");
		locationState.put("WASHINGTON DC", "DC");
		locationState.put("WILMINGTON DE", "DE");
		locationState.put("DAYTONA BEACH", "FL");
		locationState.put("JACKSONVILLE", "FL");
		locationState.put("KEY WEST", "FL");
		locationState.put("MIAMI BEACH", "FL");
		locationState.put("ORLANDO", "FL");
		locationState.put("ST PETERSBURG", "FL");
		locationState.put("TALLAHASSEE", "FL");
		locationState.put("TAMPA", "FL");
		locationState.put("W PALM BEACH", "FL");
		locationState.put("ATLANTA", "GA");
		locationState.put("COLUMBUS GA", "GA");
		locationState.put("GREEN BAY", "GA");
		locationState.put("MACON", "GA");
		locationState.put("SAVANNAH", "GA");
		locationState.put("HONOLULU", "HI");
		locationState.put("DES MOINES", "IA");
		locationState.put("SIOUX CITY", "IA");
		locationState.put("BOISE", "ID");
		locationState.put("POCATELLO", "ID");
		locationState.put("CHICAGO", "IL");
		locationState.put("EVANSVILLE", "IL");
		locationState.put("PEORIA", "IL");
		locationState.put("ROCKFORD", "IL");
		locationState.put("SPRINGFIELD IL", "IL");
		locationState.put("FORT WAYNE", "IN");
		locationState.put("INDIANAPOLIS", "IN");
		locationState.put("SOUTH BEND", "IN");
		locationState.put("TOPEKA", "KA");
		locationState.put("GOODLAND", "KS");
		locationState.put("KANSAS CITY", "KS");
		locationState.put("WICHITA", "KS");
		locationState.put("LEXINGTON", "KY");
		locationState.put("PADUCAH", "KY");
		locationState.put("BATON ROUGE", "LA");
		locationState.put("LAKE CHARLES", "LA");
		locationState.put("NEW ORLEANS", "LA");
		locationState.put("SHREVEPORT", "LA");
		locationState.put("BOSTON", "MA");
		locationState.put("BALTIMORE", "MD");
		locationState.put("PORTLAND ME", "ME");
		locationState.put("DETROIT", "MI");
		locationState.put("FLINT", "MI");
		locationState.put("GRAND RAPIDS", "MI");
		locationState.put("LANSING", "MI");
		locationState.put("DULUTH", "MN");
		locationState.put("MPLS ST PAUL", "MN");
		locationState.put("SANTA FE", "MO");
		locationState.put("SPRINGFIELD MO", "MO");
		locationState.put("ST LOUIS", "MO");
		locationState.put("JACKSON MS", "MS");
		locationState.put("TUPELO", "MS");
		locationState.put("GREAT FALLS", "MT");
		locationState.put("HELENA", "MT");
		locationState.put("MISSOULA", "MT");
		locationState.put("ASHEVILLE", "NC");
		locationState.put("CHARLOTTE", "NC");
		locationState.put("GREENSBORO", "NC");
		locationState.put("RALEIGH DURHAM", "NC");
		locationState.put("BISMARCK", "ND");
		locationState.put("FARGO", "ND");
		locationState.put("LOUISVILLE", "NE");
		locationState.put("NORTH PLATTE", "NE");
		locationState.put("OMAHA", "NE");
		locationState.put("CONCORD NH", "NH");
		locationState.put("ATLANTIC CITY", "NJ");
		locationState.put("NEWARK", "NJ");
		locationState.put("ALBUQUERQUE", "NM");
		locationState.put("RENO", "NV");
		locationState.put("ALBANY NY", "NY");
		locationState.put("BUFFALO", "NY");
		locationState.put("NEW YORK CITY", "NY");
		locationState.put("ROCHESTER NY", "NY");
		locationState.put("SYRACUSE", "NY");
		locationState.put("AKRON CANTON", "OH");
		locationState.put("CINCINNATI", "OH");
		locationState.put("CLEVELAND", "OH");
		locationState.put("COLUMBUS OH", "OH");
		locationState.put("DAYTON", "OH");
		locationState.put("TOLEDO", "OH");
		locationState.put("YOUNGSTOWN", "OH");
		locationState.put("MEDFORD", "OK");
		locationState.put("OKLAHOMA CITY", "OK");
		locationState.put("TULSA", "OK");
		locationState.put("EUGENE", "OR");
		locationState.put("PENDLETON", "OR");
		locationState.put("PORTLAND OR", "OR");
		locationState.put("SALEM OR", "OR");
		locationState.put("ALLENTOWN", "PA");
		locationState.put("ERIE", "PA");
		locationState.put("HARRISBURG", "PA");
		locationState.put("PHILADELPHIA", "PA");
		locationState.put("PITTSBURGH", "PA");
		locationState.put("WILKES BARRE", "PA");
		locationState.put("SAN JUAN PR", "PR");
		locationState.put("PROVIDENCE", "RI");
		locationState.put("CHARLESTON SC", "SC");
		locationState.put("COLUMBIA SC", "SC");
		locationState.put("RAPID CITY", "SD");
		locationState.put("SIOUX FALLS", "SD");
		locationState.put("CHATTANOOGA", "TN");
		locationState.put("KNOXVILLE", "TN");
		locationState.put("MEMPHIS", "TN");
		locationState.put("NASHVILLE", "TN");
		locationState.put("ABILENE TX", "TX");
		locationState.put("AMARILLO", "TX");
		locationState.put("AUSTIN", "TX");
		locationState.put("BROWNSVILLE", "TX");
		locationState.put("CORPUS CHRISTI", "TX");
		locationState.put("DALLAS FT WORTH", "TX");
		locationState.put("EL PASO", "TX");
		locationState.put("HOUSTON INTCNTL", "TX");
		locationState.put("LUBBOCK", "TX");
		locationState.put("MIDLAND ODESSA", "TX");
		locationState.put("MOBILE", "TX");
		locationState.put("SAN ANGELO", "TX");
		locationState.put("SAN ANTONIO", "TX");
		locationState.put("WACO", "TX");
		locationState.put("WICHITA FALLS", "TX");
		locationState.put("SALT LAKE CITY", "UT");
		locationState.put("NORFOLK VA", "VA");
		locationState.put("RICHMOND", "VA");
		locationState.put("ROANOKE", "VA");
		locationState.put("ST THOMAS VI", "VI");
		locationState.put("BURLINGTON VT", "VT");
		locationState.put("SEATTLE", "WA");
		locationState.put("SPOKANE", "WA");
		locationState.put("YAKIMA", "WA");
		locationState.put("MADISON", "WI");
		locationState.put("MILWAUKEE", "WI");
		locationState.put("CHARLESTON WV", "WV");
		locationState.put("CASPER", "WY");
		locationState.put("CHEYENNE", "WY");
				
		final Map<String, TimeZone> timeZoneDefs = new HashMap<String, TimeZone>();
		for(final Map.Entry<String, String> entry : locationState.entrySet())
			timeZoneDefs.put(entry.getKey(), stateTimeZone.get(entry.getValue()));
		
		this.timeZoneDefs = Collections.unmodifiableMap(timeZoneDefs);
		this.defaultTimeZone = easternTime;
	}
	
	public TimeZone forLocation(final String location)
	{
		return timeZoneDefs.containsKey(location) ? timeZoneDefs.get(location) : defaultTimeZone;
	}
}
