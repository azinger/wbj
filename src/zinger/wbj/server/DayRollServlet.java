package zinger.wbj.server;

import com.google.appengine.api.taskqueue.*;

import com.google.common.base.*;
import com.google.common.collect.*;

import java.io.*;
import java.util.*;
import java.util.logging.*;

import javax.mail.*;
import javax.mail.internet.*;
import javax.servlet.*;
import javax.servlet.http.*;

import zinger.wbj.client.*;
import zinger.wbj.db.*;
import zinger.wbj.lookup.*;

public class DayRollServlet extends HttpServlet
{
	protected final Logger log = Logger.getLogger(this.getClass().getName());
	
	public static final String FROM_EMAIL = "admin@weatherblackjack.appspotmail.com";
	public static final String FROM_NAME = "Weather Black Jack";
	
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException
	{
		LookupManager.INSTANCE.getLookup().rollDay();
		
		this.sendNotifications();
	}
	
	protected void sendNotifications()
	{
		final Calendar cal = Calendar.getInstance();
		final int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
		final String dayOfWeekStr = String.valueOf(dayOfWeek);
		
		final BackEndService backEnd = new BackEndServiceImpl();
		final Lookup lookup = LookupManager.INSTANCE.getLookup();
		final StateManager stateManager = StateManagerFactory.INSTANCE.getStateManager();
		final Session mailSession = Session.getDefaultInstance(new Properties());
		
		final com.google.appengine.api.taskqueue.Queue taskQueue = QueueFactory.getDefaultQueue();
		
		for(final String location : lookup.queryLocation(""))
		{
			taskQueue.add(TaskOptions.Builder
				.withUrl("/job/tally_pool")
				.method(TaskOptions.Method.GET)
				.param(TallyPoolServlet.LOCATION_PARAM, location)
				.param(TallyPoolServlet.DAY_OF_WEEK_PARAM, dayOfWeekStr)
			);
			
			try
			{
				final Winnings winnings = backEnd.getWinnersByLocation(location);
				final Set<String> winners = new HashSet<String>(Collections2.transform(winnings.getWinners(), new Function<Wager, String>()
				{
					public String apply(final Wager wager)
					{
						return wager.getUser();
					}
				}));
				
				final Map<String, Number> bets = stateManager.getBetsByLocation(location);
				for(final Map.Entry<String, Number> betEntry : bets.entrySet())
				{
					try
					{
						if(winners.contains(betEntry.getKey()))
							this.sendWinningEmail(mailSession, winnings, betEntry.getKey(), stateManager.getUserEmail(betEntry.getKey()), betEntry.getValue().intValue());
						else
							this.sendLosingEmail(mailSession, winnings, betEntry.getKey(), stateManager.getUserEmail(betEntry.getKey()), betEntry.getValue().intValue());
					}
					catch(final MessagingException ex)
					{
						ex.printStackTrace();
					}
					catch(final UnsupportedEncodingException ex)
					{
						ex.printStackTrace();
					}
					catch(final RuntimeException ex)
					{
						ex.printStackTrace();
					}
				}
			}
			catch(final NotFoundException ex)
			{
				ex.printStackTrace();
			}
		}
	}
	
	protected void sendWinningEmail(final Session mailSession, final Winnings winnings, final String user, final String userEmail, final int bet)
		throws MessagingException, UnsupportedEncodingException
	{
		final Message message = new MimeMessage(mailSession);
		message.setSentDate(new Date());
		message.setFrom(new InternetAddress(FROM_EMAIL, FROM_NAME));
		message.addRecipient(Message.RecipientType.TO, new InternetAddress(userEmail, user));
		message.setSubject(String.format("You won in %s!", winnings.getObserved().getLocation()));
		message.setText(String.format(
			"Dear %s,\n" +
			"You bet the high of %d in %s and you won, given actual observed high of %d against %d other %s.\n" +
			"Congratulations!",
			user, 
			bet, 
			winnings.getObserved().getLocation(),
			winnings.getObserved().getHigh(),
			winnings.getTotalSubmitted() - 1,
			winnings.getTotalSubmitted() == 2 ? "player" : "players"
		));
		Transport.send(message);
	}
	
	protected void sendLosingEmail(final Session mailSession, final Winnings winnings, final String user, final String userEmail, final int bet)
		throws MessagingException, UnsupportedEncodingException
	{
		final Message message = new MimeMessage(mailSession);
		message.setSentDate(new Date());
		message.setFrom(new InternetAddress(FROM_EMAIL, FROM_NAME));
		message.addRecipient(Message.RecipientType.TO, new InternetAddress(userEmail, user));
		message.setSubject(String.format("Thanks for playing in %s", winnings.getObserved().getLocation()));
		message.setText(String.format(
			"Dear %s,\n" +
			"You bet the high of %d in %s and you lost, given actual observed high of %d against %d other %s.\n" +
			"Thanks for trying, hope to see you soon!",
			user, 
			bet, 
			winnings.getObserved().getLocation(),
			winnings.getObserved().getHigh(),
			winnings.getTotalSubmitted() - 1,
			winnings.getTotalSubmitted() == 2 ? "player" : "players"
		));
		Transport.send(message);
	}
}
