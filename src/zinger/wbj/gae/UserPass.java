package zinger.wbj.gae;

import com.google.common.base.*;

import java.util.*;

import javax.jdo.*;
import javax.jdo.annotations.*;


@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class UserPass
{
	@PrimaryKey
	@Persistent
	private String user;
	
	@Persistent
	private int passHash;
	
	@Persistent
	private String email;
	
	public UserPass(final String user, final int passHash, final String email)
	{
		this.user = user;
		this.passHash = passHash;
		this.email = email;
	}
	
	public String getUser() { return user; }
	public void setUser(final String user) { this.user = user; }
	
	public int getPassHash() { return passHash; }
	public void setPassHash(final int passHash) { this.passHash = passHash; }
	
	public String getEmail() { return email; }
	public void setEmail(final String email) { this.email = email; }
	
	public int hashCode()
	{
		return com.google.common.base.Objects.hashCode(getUser(), getPassHash(), getEmail());
	}
	
	public boolean equals(final Object obj)
	{
		if(!(obj instanceof UserPass))
			return false;
		
		final UserPass other = (UserPass)obj;
		return 
			com.google.common.base.Objects.equal(this.getUser(), other.getUser()) &&
			this.getPassHash() == other.getPassHash() &&
			com.google.common.base.Objects.equal(this.getEmail(), other.getEmail());
	}
}
