package zinger.wbj.gae;

import com.google.appengine.api.datastore.*;

import com.google.common.base.*;

import java.io.*;
import java.util.*;

import javax.jdo.*;
import javax.jdo.annotations.*;


@PersistenceCapable
public class Bet
{
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private com.google.appengine.api.datastore.Key key;
	
	@Persistent
	private boolean current;
	
	@Persistent
	private String user;
	
	@Persistent
	private String location;
	
	@Persistent
	private int high;
	
	public Bet(final boolean current, final String user, final String location, final int high)
	{
		this.current = current;
		this.user = user;
		this.location = location;
		this.high = high;
	}
	
	public com.google.appengine.api.datastore.Key getKey() { return key; }
	
	public boolean isCurrent() { return current; }
	public void setCurrent(final boolean current) { this.current = current; }
	
	public String getUser() { return user; }
	public void setUser(final String user) { this.user = user; }
	
	public String getLocation() { return location; }
	public void setLocation(final String location) { this.location = location; }
	
	public int getHigh() { return high; }
	public void setHigh(final int high) { this.high = high; }
	
	public int hashCode()
	{
		return com.google.common.base.Objects.hashCode(isCurrent(), getUser(), getLocation(), getHigh());
	}
	
	public boolean equals(final Object obj)
	{
		if(!(obj instanceof Bet))
			return false;
		
		final Bet other = (Bet)obj;
		return 
			this.isCurrent() == other.isCurrent() &&
			com.google.common.base.Objects.equal(this.getUser(), other.getUser()) &&
			com.google.common.base.Objects.equal(this.getLocation(), other.getLocation()) &&
			this.getHigh() == other.getHigh();
	}
	
	public String toString()
	{
		return com.google.common.base.Objects.toStringHelper(this)
			.add("key", getKey())
			.add("current", isCurrent())
			.add("user", getUser())
			.add("location", getLocation())
			.add("high", getHigh())
			.toString();
	}
}
