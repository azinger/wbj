package zinger.wbj.gae;

import com.google.appengine.api.datastore.*;

import com.google.common.base.*;

import java.io.*;
import java.util.*;

import javax.jdo.*;
import javax.jdo.annotations.*;

@PersistenceCapable
public class ObservedHigh
{
	@PrimaryKey
	@Persistent
	private String location;
	
	@Persistent
	private int high;
	
	@Persistent
	private Date timestamp;
	
	public ObservedHigh(final String location, final int high, final Date timestamp)
	{
		this.location = location;
		this.high = high;
		this.timestamp = timestamp;
	}
	
	public String getLocation() { return location; }
	public int getHigh() { return high; }
	public Date getTimestamp() { return timestamp; }
	
	public int hashCode()
	{
		return com.google.common.base.Objects.hashCode(getLocation(), getHigh(), getTimestamp());
	}
	
	public boolean equals(final Object obj)
	{
		if(!(obj instanceof ObservedHigh))
			return false;
		
		final ObservedHigh other = (ObservedHigh)obj;
		return 
			com.google.common.base.Objects.equal(this.getLocation(), other.getLocation()) &&
			this.getHigh() == other.getHigh() &&
			com.google.common.base.Objects.equal(this.getTimestamp(), other.getTimestamp());
	}
	
	public String toString()
	{
		return com.google.common.base.Objects.toStringHelper(this)
			.add("location", getLocation())
			.add("high", getHigh())
			.add("timestamp", getTimestamp())
			.toString();
	}
}
