package zinger.wbj.gae;

import com.google.appengine.api.datastore.*;

import com.google.common.base.*;

import java.io.*;
import java.util.*;

import javax.jdo.*;
import javax.jdo.annotations.*;

@PersistenceCapable
public class Invite
{
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private com.google.appengine.api.datastore.Key key;
	
	@Persistent
	private String author;
	
	@Persistent
	private String invitedEmail;
	
	@Persistent
	private String invitedToPool;
	
	@Persistent
	private Date invitedOn;
	
	public Invite(final String author, final String invitedEmail, final String invitedToPool, final Date invitedOn)
	{
		this.author = author;
		this.invitedEmail = invitedEmail;
		this.invitedToPool = invitedToPool;
		this.invitedOn = invitedOn;
	}
	
	public String getAuthor() { return author; }
	public void setAuthor(final String author) { this.author = author; }
	
	public String getInvitedEmail() { return invitedEmail; }
	public void setInvitedEmail(final String invitedEmail) { this.invitedEmail = invitedEmail; }
	
	public String getInvitedToPool() { return invitedToPool; }
	public void setInvitedToPool(final String invitedToPool) { this.invitedToPool = invitedToPool; }
	
	public Date getInvitedOn() { return invitedOn; }
	public void setInvitedOn(final Date invitedOn) { this.invitedOn = invitedOn; }
	
	@Override
	public int hashCode()
	{
		return com.google.common.base.Objects.hashCode(getAuthor(), getInvitedEmail(), getInvitedToPool(), getInvitedOn());
	}
	
	@Override
	public boolean equals(final Object obj)
	{
		if(!(obj instanceof Invite))
			return false;
		final Invite other = (Invite)obj;
		return
			com.google.common.base.Objects.equal(this.getAuthor(), other.getAuthor()) &&
			com.google.common.base.Objects.equal(this.getInvitedEmail(), other.getInvitedEmail()) &&
			com.google.common.base.Objects.equal(this.getInvitedToPool(), other.getInvitedToPool()) &&
			com.google.common.base.Objects.equal(this.getInvitedOn(), other.getInvitedOn());
	}
	
	@Override
	public String toString()
	{
		return com.google.common.base.Objects.toStringHelper(this)
			.add("author", getAuthor())
			.add("invitedEmail", getInvitedEmail())
			.add("invitedToPool", getInvitedToPool())
			.add("invitedOn", getInvitedOn())
			.toString();
	}
}
