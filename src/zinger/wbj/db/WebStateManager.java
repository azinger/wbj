package zinger.wbj.db;

import javax.servlet.*;

public abstract class WebStateManager implements StateManager, ServletContextListener
{
	public void contextInitialized(final ServletContextEvent ev)
	{
		StateManagerFactory.INSTANCE.stateManager = this;
	}
	
	public void contextDestroyed(final ServletContextEvent ev)
	{
		if(StateManagerFactory.INSTANCE.stateManager == this)
			StateManagerFactory.INSTANCE.stateManager = null;
	}
}
