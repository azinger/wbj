package zinger.wbj.db;

import com.google.appengine.api.datastore.Key;

import com.google.common.base.*;
import com.google.common.collect.*;

import java.util.*;
import java.util.logging.*;

import javax.mail.*;
import javax.mail.internet.*;

import javax.jdo.*;
import javax.servlet.*;

import zinger.wbj.client.*;
import zinger.wbj.gae.*;

/**
 * State persistence implementation backed by JDO tailored to GAE.
 */
public class JdoStateManager extends WebStateManager
{
	protected final Logger log = Logger.getLogger(this.getClass().getName());
	protected PersistenceManagerFactory pmf;
	
	/////////////////////////////////////////////////////
	// StateManager impl
	
	public void contextInitialized(final ServletContextEvent ev)
	{
		this.pmf = JDOHelper.getPersistenceManagerFactory("transactions-optional");
		
		super.contextInitialized(ev);
	}
	
	public void contextDestroyed(final ServletContextEvent ev)
	{
		super.contextDestroyed(ev);
		if(this.pmf != null)
			this.pmf.close();
	}
	
	protected UserPass getUserPass(final String user) throws NotFoundException
	{
		final PersistenceManager pm = pmf.getPersistenceManager();
		final Query query = pm.newQuery(UserPass.class, "this.user == user");
		query.declareParameters("String user");
		final Collection<UserPass> userPasses = (Collection<UserPass>)query.execute(user);
		if(userPasses.isEmpty())
			throw new NotFoundException(String.format("User %s not found.", user));
		final UserPass result = userPasses.iterator().next();
		try
		{
			return pm.detachCopy(result);
		}
		finally
		{
			pm.close();
		}
	}
	
	public int getUserPassHash(final String user) throws NotFoundException
	{
		// TODO: need to take out authentication
		return getUserPass(user).getPassHash();
	}
	
	public synchronized void setUserPassHashMulti(final Map<String, Integer> userPassHashMap)
	{
		// TODO: need to take out authentication
		final PersistenceManager pm = this.pmf.getPersistenceManager();
		pm.currentTransaction().begin();
		pm.makePersistentAll(Collections2.transform(userPassHashMap.entrySet(), new Function<Map.Entry<String, Integer>, UserPass>()
		{
			public UserPass apply(final Map.Entry<String, Integer> entry)
			{
				return new UserPass(entry.getKey(), entry.getValue(), null);
			}
		}));
		pm.currentTransaction().commit();
		pm.close();
	}
	
	public String getUserEmail(final String user) throws NotFoundException
	{
		try
		{
			return getUserPass(user).getEmail();
		}
		catch(final NotFoundException userNotFoundEx)
		{
			try
			{
				String addr = user;
				if(!addr.contains("@"))
					addr += "@gmail.com";
				new InternetAddress(addr);
				return addr;
			}
			catch(final AddressException addrEx)
			{
				throw userNotFoundEx;
			}
		}
	}
	
	public void setUserEmail(final String user, final String email)
	{
		final PersistenceManager pm = this.pmf.getPersistenceManager();
		pm.currentTransaction().begin();
		try
		{
			getUserPass(user).setEmail(email);
		}
		catch(final NotFoundException ex)
		{
			pm.makePersistent(new UserPass(user, 0, email));
		}
		pm.currentTransaction().commit();
		pm.close();
	}
	
	public Set<String> getUserEmails()
	{
		final PersistenceManager pm = pmf.getPersistenceManager();
		final Query query = pm.newQuery(UserPass.class);
		try
		{
			final Collection<UserPass> users = (Collection<UserPass>)query.execute();
			final Set<String> emails = new HashSet<String>(users.size());
			for(final UserPass user : users)
				emails.add(user.getEmail());
			return emails;
		}
		finally
		{
			pm.close();
		}
	}
	
	public synchronized void clearBets()
	{
		final PersistenceManager pm = this.pmf.getPersistenceManager();
		final Query query = pm.newQuery(Bet.class, "this.current == current");
		query.declareParameters("boolean current");
		
		final Collection<Bet> yesterdayBets = (Collection<Bet>)query.execute(false);
		final Collection<Key> yesterdayBetKeys = new HashSet<Key>(Collections2.transform(yesterdayBets, new Function<Bet, Key>()
		{
			public Key apply(final Bet bet)
			{
				return bet.getKey();
			}
		}));
		
		final Query keyQuery = pm.newQuery(Bet.class, "this.key == key");
		keyQuery.declareParameters("com.google.appengine.api.datastore.Key key");
		for(final Key betKey : yesterdayBetKeys)
		{
			pm.currentTransaction().begin();
			keyQuery.deletePersistentAll(betKey);
			pm.currentTransaction().commit();
		}
		
		final Collection<Bet> todayBets = pm.detachCopyAll((Collection<Bet>)query.execute(true));
		for(final Bet bet : todayBets)
		{
			pm.currentTransaction().begin();
			bet.setCurrent(false);
			pm.makePersistent(bet);
			pm.currentTransaction().commit();
		}
		
		pm.close();
	}
	
	public Map<String, Number> getBetsByUser(final String user)
	{
		final PersistenceManager pm = this.pmf.getPersistenceManager();
		final Query query = pm.newQuery(Bet.class, "this.current == true && this.user == user");
		query.declareParameters("String user");
		
		final Map<String, Number> userBets = new HashMap<String, Number>();
		for(final Bet bet : (Collection<Bet>)query.execute(user))
			userBets.put(bet.getLocation(), bet.getHigh());
		
		pm.close();
		return userBets;
	}
	
	public Map<String, Number> getBetsByLocation(final String location)
	{
		return getBetsByLocation(location, false);
	}
	
	public Map<String, Number> getBetsByLocation(final String location, final boolean current)
	{
		final PersistenceManager pm = this.pmf.getPersistenceManager();
		final Query query = pm.newQuery(Bet.class, "this.current == current && this.location == location");
		query.declareParameters("String location, boolean current");
		
		final Map<String, Number> locationBets = new HashMap<String, Number>();
		for(final Bet bet : (Collection<Bet>)query.execute(location, current))
			locationBets.put(bet.getUser(), bet.getHigh());
		
		pm.close();
		return locationBets;
	}
	
	public synchronized boolean submitBet(final String user, final String location, final int high)
	{
		if(user == null || location == null)
			return false;
			
		final PersistenceManager pm = this.pmf.getPersistenceManager();
		try
		{
			pm.currentTransaction().begin();
			final Query query = pm.newQuery(Bet.class, "this.current == true && this.user == user && this.location == location");
			query.declareParameters("String user, String location");
			final Collection<Bet> bets = (Collection<Bet>)query.execute(user, location);
			if(!bets.isEmpty())
			{
				pm.currentTransaction().rollback();
				return false;
			}

			pm.makePersistent(new Bet(true, user, location, high));
			pm.currentTransaction().commit();
			return true;
		}
		finally
		{
			pm.close();
		}
	}
	
	public Set<String> getBetLocationsByUser(final String user)
	{
		final PersistenceManager pm = this.pmf.getPersistenceManager();
		final Query query = pm.newQuery(Bet.class, "this.current == false && this.user == user");
		query.declareParameters("String user");
		
		final Collection<Bet> bets = (Collection<Bet>)query.execute(user);
		try
		{
			return new HashSet<String>(Collections2.transform(bets, new Function<Bet, String>()
			{
				public String apply(final Bet bet)
				{
					return bet.getLocation();
				}
			}));
		}
		finally
		{
			pm.close();
		}
	}
	
	public Set<String> getUserPools(final String user)
	{
		final PersistenceManager pm = this.pmf.getPersistenceManager();
		final Query query = pm.newQuery(UserPool.class, "this.user == user");
		query.declareParameters("String user");
		try
		{
			return new HashSet<String>(
				Collections2.transform(pm.detachCopyAll((Collection<UserPool>)query.execute(user)), new Function<UserPool, String>()
				{
					public String apply(final UserPool userPool)
					{
						return userPool.getPool();
					}
				})
			);
		}
		finally
		{
			pm.close();
		}
	}
	
	public synchronized void addPool(final String user, final String pool) throws NotFoundException
	{
		if(getUserPools(user).contains(pool))
			return;
		final PersistenceManager pm = this.pmf.getPersistenceManager();
		pm.currentTransaction().begin();
		pm.makePersistent(new UserPool(user, pool));
		pm.currentTransaction().commit();
		pm.close();
	}
	
	public synchronized void removePool(final String user, final String pool)
	{
		final PersistenceManager pm = this.pmf.getPersistenceManager();
		pm.currentTransaction().begin();
		final Query query = pm.newQuery(UserPool.class, "this.user == user && this.pool == pool");
		query.declareParameters("String user, String pool");
		for(final UserPool userPool : (Collection<UserPool>)query.execute(user, pool))
			pm.deletePersistent(userPool);
		pm.currentTransaction().commit();
		pm.close();
	}
	
	public Set<String> getUsersInPool(final String pool)
	{
		final PersistenceManager pm = this.pmf.getPersistenceManager();
		final Query query = pm.newQuery(UserPool.class, "this.pool == pool");
		query.declareParameters("String pool");
		try
		{
			return new HashSet<String>(
				Collections2.transform(pm.detachCopyAll((Collection<UserPool>)query.execute(pool)), new Function<UserPool, String>()
				{
					public String apply(final UserPool userPool)
					{
						return userPool.getUser();
					}
				})
			);
		}
		finally
		{
			pm.close();
		}
	}
	
	public Set<String> queryPools(final String poolQuery)
	{
		final PersistenceManager pm = this.pmf.getPersistenceManager();
		final Query query = pm.newQuery(UserPool.class, "this.pool.startsWith(poolQuery)");
		query.declareParameters("String poolQuery");
		try
		{
			return new HashSet<String>(
				Collections2.transform(pm.detachCopyAll((Collection<UserPool>)query.execute(poolQuery)), new Function<UserPool, String>()
				{
					public String apply(final UserPool userPool)
					{
						return userPool.getPool();
					}
				})
			);
		}
		finally
		{
			pm.close();
		}
	}
	
	public Set<String> getAllPools()
	{
		final PersistenceManager pm = this.pmf.getPersistenceManager();
		final Query query = pm.newQuery(UserPool.class);
		try
		{
			return new HashSet<String>(
				Collections2.transform((Collection<UserPool>)query.executeWithArray(), new Function<UserPool, String>()
				{
					public String apply(final UserPool userPool)
					{
						return userPool.getPool();
					}
				})
			);
		}
		finally
		{
			pm.close();
		}
	}
	
	public Map<String, Integer> getObservedHighs()
	{
		final PersistenceManager pm = this.pmf.getPersistenceManager();
		try
		{
			final Query query = pm.newQuery(ObservedHigh.class);
			final Map<String, Integer> map = new HashMap<String, Integer>();
			for(final ObservedHigh observedHigh : (Collection<ObservedHigh>)query.execute(false))
				map.put(observedHigh.getLocation(), observedHigh.getHigh());
			this.log.info("Observed highs: " + map);
			return map;
		}
		finally
		{
			pm.close();
		}
	}
	
	public void setObservedHighs(final Map<String, Integer> observedHighsMap)
	{
		this.log.info("Observed highs: " + observedHighsMap);
		final PersistenceManager pm = this.pmf.getPersistenceManager();
		try
		{
			final Query locationQuery = pm.newQuery(ObservedHigh.class, "this.location == location");
			locationQuery.declareParameters("String location");
			for(final String location : this.getObservedHighs().keySet())
			{
				pm.currentTransaction().begin();
				locationQuery.deletePersistentAll(location);
				pm.currentTransaction().commit();
			}
			
			final Date timestamp = new Date();
			for(final Map.Entry<String, Integer> observedHighEntry : observedHighsMap.entrySet())
			{
				final ObservedHigh observedHigh = new ObservedHigh(observedHighEntry.getKey(), observedHighEntry.getValue(), timestamp);
				pm.currentTransaction().begin();
				pm.makePersistent(observedHigh);
				pm.currentTransaction().commit();
			}
		}
		finally
		{
			pm.close();
		}
	}
	
	public void addUserTally(final String user, final String location, final String pool, final int dayOfWeek, final float userTally)
	{
		final PersistenceManager pm = this.pmf.getPersistenceManager();
		pm.currentTransaction().begin();
		pm.makePersistent(new PoolTally(user, location, pool, dayOfWeek, userTally));
		pm.currentTransaction().commit();
		pm.close();
	}
	
	public float getUserTally(final String user, final String pool)
	{
		float tally = 0F;
		final PersistenceManager pm = this.pmf.getPersistenceManager();
		final Query query = pm.newQuery(PoolTally.class, "this.user == user && this.pool == pool");
		query.declareParameters("String user, String pool");
		for(final PoolTally poolTally : (Collection<PoolTally>)query.execute(user, pool))
			tally += poolTally.getTally();
		pm.close();
		return tally;
	}
	
	public void clearUserTallies(final String user, final String pool)
	{
		final PersistenceManager pm = this.pmf.getPersistenceManager();
		final Query tallyQuery = pm.newQuery(PoolTally.class, "this.user == user && this.pool == pool");
		tallyQuery.declareParameters("String user, String pool");
		final Collection<PoolTally> tallies = pm.detachCopyAll((Collection<PoolTally>)tallyQuery.execute(user, pool));
		

		final Query delQuery = pm.newQuery(PoolTally.class, "this.key == key");
		delQuery.declareParameters("String key");
		for(final PoolTally tally : tallies)
		{
			pm.currentTransaction().begin();
			delQuery.deletePersistentAll(tally.getKey());
			pm.currentTransaction().commit();
		}
	}
	
	public Map<String, Set<String>> getInvitesForAuthor(final String authorUser)
	{
		final SetMultimap<String, String> invitedToPools = HashMultimap.create();
		
		final PersistenceManager pm = this.pmf.getPersistenceManager();
		final Query inviteQuery = pm.newQuery(Invite.class, "this.author == author");
		inviteQuery.declareParameters("String author");
		for(final Invite invite : (Collection<Invite>)inviteQuery.execute(authorUser))
			invitedToPools.put(invite.getInvitedEmail(), invite.getInvitedToPool());
		
		final Map<String, Set<String>> result = Maps.transformValues(invitedToPools.asMap(), new Function<Collection<String>, Set<String>>()
		{
			public Set<String> apply(final Collection<String> pools)
			{
				return (Set<String>)pools;
			}
		});
		return new HashMap<String, Set<String>>(result);
	}
	
	public Set<String> getInvitesForEmail(final String invitedEmail)
	{
		final Set<String> invitedToPools = new HashSet<String>();
		
		final PersistenceManager pm = this.pmf.getPersistenceManager();
		final Query inviteQuery = pm.newQuery(Invite.class, "this.invitedEmail == invitedEmail");
		inviteQuery.declareParameters("String invitedEmail");
		for(final Invite invite : (Collection<Invite>)inviteQuery.execute(invitedEmail))
			invitedToPools.add(invite.getInvitedToPool());
		
		return invitedToPools;
	}
	
	public void dismissInvites(final String invitedEmail, final Set<String> invitedToPools)
	{
		final PersistenceManager pm = this.pmf.getPersistenceManager();
		final Query query = pm.newQuery(Invite.class, "this.invitedEmail == invitedEmail && this.invitedToPool == invitedToPool");
		query.declareParameters("String invitedEmail, String invitedToPool");
		for(final String invitedToPool : invitedToPools)
		{
			for(final Invite invite : (Collection<Invite>)query.execute(invitedEmail, invitedToPool))
			{
				pm.currentTransaction().begin();
				pm.deletePersistent(invite);
				pm.currentTransaction().commit();
			}
		}
		pm.close();
	}
	
	public void addInvite(final String authorUser, final String invitedEmail, final String invitedToPool) throws IllegalArgumentException
	{
		final Map<String, Set<String>> existingInvites = getInvitesForAuthor(authorUser);
		if(existingInvites.containsKey(invitedEmail) && existingInvites.get(invitedEmail).contains(invitedToPool))
			throw new IllegalArgumentException("This invite has already been entered");
		
		final PersistenceManager pm = this.pmf.getPersistenceManager();
		pm.currentTransaction().begin();
		pm.makePersistent(new Invite(authorUser, invitedEmail, invitedToPool, new Date()));
		pm.currentTransaction().commit();
		pm.close();
	}
	
	// StateManager impl
	/////////////////////////////////////////////////////
}
