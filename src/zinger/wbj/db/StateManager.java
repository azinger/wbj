package zinger.wbj.db;

import java.util.*;

import zinger.wbj.client.*;

public interface StateManager
{
	//public List<Wager> getWagersByLocation(String location);
	//public List<Wager> getWagersByUser(String user);
	
	public int getUserPassHash(String user) throws NotFoundException;
	public void setUserPassHashMulti(Map<String, Integer> userPassHashMap);
	public String getUserEmail(String user) throws NotFoundException;
	public void setUserEmail(String user, String email);
	
	public Set<String> getUserEmails();
	
	public void clearBets();
	public Map<String, Number> getBetsByUser(String user);
	public Map<String, Number> getBetsByLocation(String location);
	public Map<String, Number> getBetsByLocation(String location, boolean current);
	public boolean submitBet(String user, String location, int high);
	public Set<String> getBetLocationsByUser(String user);
	
	public Set<String> getUserPools(String user) throws NotFoundException;
	public void addPool(String user, String pool) throws NotFoundException;
	public void removePool(String user, String pool);
	public Set<String> getUsersInPool(String pool);
	public Set<String> queryPools(String poolQuery);
	public Set<String> getAllPools();
	
	public Map<String, Integer> getObservedHighs();
	public void setObservedHighs(Map<String, Integer> observedHighs);
	
	public void addUserTally(String user, String location, String pool, int dayOfWeek, float userTally);
	public float getUserTally(String user, String pool);
	public void clearUserTallies(String user, String pool);
	
	public Map<String, Set<String>> getInvitesForAuthor(String authorUser);
	public Set<String> getInvitesForEmail(String invitedEmail);
	public void dismissInvites(String invitedEmail, Set<String> invitedToPools);
	public void addInvite(String authorUser, String invitedEmail, String invitedToPool) throws IllegalArgumentException;
}
