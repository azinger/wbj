package zinger.wbj.lookup;

import com.google.common.base.*;
import com.google.common.collect.*;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.logging.*;
import java.util.regex.*;

import javax.servlet.*;

import org.apache.http.*;
import org.apache.http.client.*;
import org.apache.http.client.methods.*;
import org.apache.http.impl.client.*;

import zinger.wbj.client.*;
import zinger.wbj.db.*;

public class WeatherGovLookup implements Lookup, ServletContextListener
{
	protected final HttpClient client = new DefaultHttpClient();
	protected final Logger log = Logger.getLogger(this.getClass().getName());
	
	protected final Map<String, Integer> observedHighs = new HashMap<String, Integer>();
	protected URI siteUri;
	protected final Pattern tempLinePattern = Pattern.compile("^(\\w+|\\w+\\s\\w+|\\w+\\s\\w+\\s\\w+)\\s\\s+(\\d+)\\s+(\\d+)");
	
	public WeatherGovLookup()
	{
		try
		{
			this.siteUri = new URI("https://www.weather.gov/xml/tpex/scs.php");
			//this.siteUri = new URI("http://localhost:8080/wbj/scs.html");
		}
		catch(final URISyntaxException ex)
		{
			this.log.log(Level.SEVERE, null, ex);
		}
	}
	
	protected void readObservedHighs(final BufferedReader reader) throws IOException
	{
		String line;
		while((line = reader.readLine()) != null)
		{
			final Matcher m = this.tempLinePattern.matcher(line);
			if(m.find())
			{
				try
				{
					final String location = m.group(1);
					final int high = Math.max(Integer.parseInt(m.group(2)), Integer.parseInt(m.group(3)));
					this.observedHighs.put(location, high);
				}
				catch(final NumberFormatException ex)
				{
					this.log.log(Level.WARNING, String.format("Could not parse temp in line %s", line), ex);
				}
			}
		}
		reader.close();
	}
	
	protected void learnObservedHighs()
	{
		this.observedHighs.clear();
		try
		{
			final HttpGet request = new HttpGet(this.siteUri);
			final HttpResponse response = this.client.execute(request);
			final HttpEntity entity = response.getEntity();
			if(entity != null)
			{
				final BufferedReader reader = new BufferedReader(new InputStreamReader(entity.getContent()));
				this.readObservedHighs(reader);
			}
		}
		catch(final IOException ex)
		{
			this.log.log(Level.WARNING, null, ex);
		}
		StateManagerFactory.INSTANCE.getStateManager().setObservedHighs(this.observedHighs);
		this.log.info(String.format("Temps read: %s", this.observedHighs));
	}
	
	protected void loadObservedHighs()
	{
		this.observedHighs.clear();
		this.observedHighs.putAll(StateManagerFactory.INSTANCE.getStateManager().getObservedHighs());
	}
	
	public void rollDay()
	{
		this.learnObservedHighs();
		StateManagerFactory.INSTANCE.getStateManager().clearBets();
	}
	
	public Set<String> queryLocation(final String locationPrefix)
	{
		final String locationPrefixUpper = locationPrefix.toUpperCase();
		return Sets.filter(this.observedHighs.keySet(), new Predicate<String>()
		{
			public boolean apply(final String location)
			{
				return location.startsWith(locationPrefixUpper);
			}
		});
	}
	
	public int get(final String location) throws NotFoundException
	{
		if(this.observedHighs.containsKey(location))
			return this.observedHighs.get(location);
		else
			throw new NotFoundException("No observed high for " + location + " found.");
	}
	
	public void contextInitialized(final ServletContextEvent ev)
	{
		//this.learnObservedHighs();
		this.loadObservedHighs();
		LookupManager.INSTANCE.setLookup(this, Boolean.parseBoolean(ev.getServletContext().getInitParameter("zinger.wbj.enableThreading")));
	}
	
	public void contextDestroyed(final ServletContextEvent ev)
	{
		LookupManager.INSTANCE.setLookup(null, false);
	}
}
