package zinger.wbj.lookup;

import java.util.*;

import zinger.wbj.client.*;

public interface Lookup
{
	public Set<String> queryLocation(String locationPrefix);
	public int get(String location) throws NotFoundException;
	public void rollDay();
}
