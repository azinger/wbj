package zinger.wbj.lookup;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.logging.*;
import java.util.regex.*;

import zinger.wbj.db.*;

public class DirectWeatherGovLookup extends WeatherGovLookup
{
	protected void learnObservedHighs()
	{
		final Map<String, Integer> observedHighsBackup = new HashMap<String, Integer>(this.observedHighs);
		this.observedHighs.clear();
		try
		{
			final InputStream is = this.siteUri.toURL().openStream();
			final BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			this.readObservedHighs(reader);
		}
		catch(final IOException ex)
		{
			this.observedHighs.putAll(observedHighsBackup);
			throw new RuntimeException(ex);
		}
		StateManagerFactory.INSTANCE.getStateManager().setObservedHighs(this.observedHighs);
		this.log.info(String.format("Temps read: %s", this.observedHighs));
	}
}
