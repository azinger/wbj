package zinger.wbj.lookup;

import java.util.*;
import java.util.logging.*;

public class LookupManager
{
	public static final LookupManager INSTANCE = new LookupManager();
	
	public static final long PERIOD = 24L * 60L * 60L * 1000L;
	
	protected final Logger log = Logger.getLogger(this.getClass().getName());
	private Timer dayRollTimer;
	
	protected Lookup lookup;
	
	protected LookupManager() {}
	
	public Lookup getLookup() 
	{ 
		return this.lookup; 
	}
	
	public void setLookup(final Lookup lookup, final boolean enableThreading) 
	{ 
		this.lookup = lookup; 
		if(enableThreading)
			this.scheduleDayRoll();
	}
	
	protected void scheduleDayRoll()
	{
		if(this.dayRollTimer == null)
		{
			this.dayRollTimer = new Timer(true);
			final Calendar cal = Calendar.getInstance();
			cal.set(Calendar.HOUR_OF_DAY, 23);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			final TimerTask task = new TimerTask()
			{
				public void run()
				{
					if(lookup != null)
					{
						log.info("Day roll...");
						lookup.rollDay();
					}
				}
			};
			final Date time = cal.getTime();
			this.dayRollTimer.scheduleAtFixedRate(task, time, PERIOD);
			this.log.info(String.format("Scheduled day roll to start %s.", time));
		}
	}
}
